package rightClick;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalElements.AllElements;

public class rightClickFileFromFolder extends TestBase {

	GlobalFanction globalfunction;
	AllElements allElements ;
	
	@BeforeClass
	public void setup() {

		this.globalfunction = new GlobalFanction();
		this.allElements = new AllElements() ;
	}
	
	
	@Test(priority = 1)
	public void renameByRightClickFileFromFolder() throws InterruptedException, FindFailed {
		
		logger = extent.createTest("rename file from folder right click"); 
		
		globalfunction.CreateFolder("mirolozOchelKalamary");

		globalfunction.EnterToFolder("mirolozOchelKalamary"); 

		globalfunction.uploadFile("file_1.png","yosef.txt") ; 

		globalfunction.renameFoldersAndFiles("yosef.txt","yaakov",true, 6);

	}
	
	@Test(priority = 2)
	public void downloadFileFromFolderByRightClick() throws InterruptedException, FindFailed {

		logger = extent.createTest("download file from folder right click"); 
		
		globalfunction.downloadFile("yaakov", true, 2);
		
	}
	
	@Test(priority = 3)
	public void shareFileFromFolderEditorPermission() throws InterruptedException, FindFailed {
		
		logger = extent.createTest(" share file from folder editor permission right click");
		
		globalfunction.sharingFileOrFolderWithEditorPermission("yaakov","אחמד אדידס","user2","user2pass", true, 4);

	}
	
	@Test(priority = 4)
	public void shareFileFromFolderReadPermission() throws InterruptedException {

		logger = extent.createTest(" share file from folder with read permission right click"); 
			
		globalfunction.sharingFileOrFolderWithReadPermission("yaakov","אחמד אדידס","user2","user2pass",true , 4);
	}
	
	@Test(priority = 5)
	public void testRemoveUserPermission() throws InterruptedException {
		
		logger = extent.createTest(" remove user permission on file from folder right click"); 
		
		globalfunction.removeUserPermission("yaakov","אחמד אדידס","user2","user2pass", true , 4);
	}
	
	@Test(priority = 6)
	public void moveFileFromFolderToMainPage() throws InterruptedException {
		
		logger = extent.createTest("move file from folder to main page right click"); 

		globalfunction.MoveFolder_FileToMainPage("mirolozOchelKalamary", "yaakov", true, 5);
	}
	
	@Test(priority = 7)
	public void deleteFileFromFolder() throws InterruptedException, FindFailed {
		
		logger = extent.createTest("delete file from folder right click"); 

		globalfunction.uploadFile("file_1.png","yosef.txt") ; 

		globalfunction.DeleteFolderOrFile("yosef.txt", true, 3);

	}
	
	@Test(priority = 8)
	public void verifyInformationFiles() throws InterruptedException, FindFailed {
		
		logger = extent.createTest("verify information on files from folder using rigth click");
		
		globalfunction.CreateFolder("folderForFile") ;
		
		globalfunction.uploadFile("file_1.png","yosef.txt") ;  
		
		globalfunction.verifyFileInformationTab("yosef.txt","אחמד אדידס","user2","user2pass", true, 0) ; 

	}
	
	
	@Test(priority = 9 )
	public void viewFileContent() throws InterruptedException, FindFailed {
		
		logger = extent.createTest(" view file content test , using right click ");
		
		globalfunction.viewFileContents("yosef.txt", true, 1) ; 
		
		allElements.MyFilesButton.click() ; 
		
		globalfunction.DeleteFolderOrFile("mirolozOchelKalamary", false, 0) ;  
	}
	
	
	
	@AfterClass
	public void after() 
	{ 
		extent.flush();
	}
	
}
