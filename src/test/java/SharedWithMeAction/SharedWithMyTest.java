package SharedWithMeAction;

import java.util.ArrayList;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;
import globalElements.AllElement1;
import globalElements.AllElements;
import net.bytebuddy.asm.Advice.AllArguments;

public class SharedWithMyTest extends TestBase 
{

		GlobalFanction globalFunction ;
		OperationsForTheOtherDriver operationsForTheOtherDriver ;
		AllElements  allElements ;

		@BeforeClass
		public void setup() throws FindFailed, InterruptedException {
			
			logger = extent.createTest("upload two files to share"); 
			
			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
			
			this.operationsForTheOtherDriver = new OperationsForTheOtherDriver() ;
			this.globalFunction = new GlobalFanction() ;
			this.allElements = new AllElements() ;
			
			operationsForTheOtherDriver.loginToOtherUser(prop.getProperty("username2"), prop.getProperty("password2")); 	
			
			operationsForTheOtherDriver.uploadFile("file_1.png","yosef.txt") ; 
			operationsForTheOtherDriver.uploadFile("file_4.png" ,"file_4.txt") ;
			
			String[] arrayFolders = {"folder1", "folder2" ,"folder3" ,"yosef.txt","file_4.txt" } ;  
			
			operationsForTheOtherDriver.CreateFolder("folder1");
			operationsForTheOtherDriver.CreateFolder("folder2");
			operationsForTheOtherDriver.CreateFolder("folder3"); 
						
			for(String folder_file : arrayFolders) {
				
				operationsForTheOtherDriver.shareFileOrFolder(folder_file, "נייקי אדידס");  
			}	
			
			driver1.quit(); 
	
			driver.switchTo().window(tabs.get(0));	
		
			Thread.sleep(2000) ; 
			
			allElements.SharedWithMeButton.click();
		}

		@Test(priority = 1)
		public void testRenameFolder() throws InterruptedException, FindFailed 
		{
			logger = extent.createTest("rename folder from share tab "); 
			
			globalFunction.renameFoldersAndFiles("folder1", "tzevetEchoot", false, 0) ; 
			   
		}
		
		@Test(priority = 2)
		public void testMoveFolderToFolder() throws InterruptedException, FindFailed 
		{
			logger = extent.createTest(" move folder to folder in share tab  "); 
			
			globalFunction.MoveFolder_FileToNewLocation("tzevetEchoot", "test3", false, 0) ;  
			    
		}
		
		
		@Test(priority = 3)
		public void moveMultipleFoldersToFolder() throws InterruptedException {
			
			logger = extent.createTest("move multiple folders from sheared tab to folder");
			
			String[] arrayFiels = {"folder2", "folder3"} ;  

			globalFunction.MoveMultipleFolder_FileToNewLocation(arrayFiels, "test3", false, 0);

		}

		
		@Test(priority = 4)
		public void shareFolderWithReadPermission() throws InterruptedException {
			
			logger = extent.createTest("sharing folder with read permission from share with me tab");
	
			allElements.SharedWithMeButton.click();
			
			globalFunction.sharingFileOrFolderWithReadPermission("test4", "תפוז כתום", "user3","user3pass" ,false , 0);
		}
		
		
		@Test(priority = 5)
		public void shareFolderWithEditorPermission() throws InterruptedException {
			
			logger = extent.createTest("sharing folder with edit permission from share with me tab");
	
			globalFunction.sharingFileOrFolderWithEditorPermission("test4", "תפוז כתום", "user3","user3pass",false , 0);
		}
		
		
		@Test(priority = 6)
		public void removeShearFolder() throws InterruptedException {
			
			logger = extent.createTest("sharing folder from share with me tab");
	
			globalFunction.removeUserPermission("test4", "תפוז כתום", "user3","user3pass",false , 0);
		}
		
		
		@Test(priority = 7)
		public void testVerifyFolderinformation() throws InterruptedException {
			
			logger = extent.createTest(" test verify folder data from share with me ") ;
			
			globalFunction.verifyFolderInformationTab("test4" ,"תפוז כתום" , "user3","user3pass" ,true,false ,0 ) ;
		}
		
		
	  	@Test(priority = 8)
		public void DeleteFolderTest() throws InterruptedException {
			
			logger = extent.createTest("move multiple folders from sheared tab to folder");
			
			globalFunction.deleteFolderOrFileFromShearedTab("test6" ,0);

		}
		
	
		
		@Test(priority = 9)
		public void switchViewTest() throws InterruptedException 
		{

			logger = extent.createTest("switchViewTest") ;
	 
			globalFunction.SwitchView();
			
		} 
		
		
		
		// tests on files 
		
		 
		@Test(priority = 10 )
		public void testDownloadFile () throws InterruptedException, FindFailed 
		{
			
			logger = extent.createTest("download file from share tab "); 
			
			globalFunction.downloadFile("yosef.txt", false , 0) ;
		}
		
		
		
		@Test(priority = 11)
		public void testrenameFile() throws InterruptedException, FindFailed 
		{
			logger = extent.createTest("rename file from share tab "); 
			
			globalFunction.renameFoldersAndFiles("yosef.txt", "israelFile", false, 0) ; 
			   
		}
		
		
		@Test(priority = 12)
		public void shareFileWithReadPermission() throws InterruptedException {
			
			logger = extent.createTest("sharing folder from share with me tab");
	
			globalFunction.sharingFileOrFolderWithReadPermission("israelFile", "תפוז כתום", "user3","user3pass",false , 0);
		}
		
		
		@Test(priority = 13)
		public void shareFileWithEditorPermission() throws InterruptedException {
			
			logger = extent.createTest("sharing folder from share with me tab");
	
			globalFunction.sharingFileOrFolderWithEditorPermission("israelFile", "תפוז כתום", "user3","user3pass",false , 0);
		}
		
		
		@Test(priority = 14)
		public void removeShearFile() throws InterruptedException {
			
			logger = extent.createTest("sharing folder from share with me tab");
	
			globalFunction.removeUserPermission("israelFile", "תפוז כתום", "user3","user3pass",false , 0);
		}
		
		
		@Test(priority = 15)
		public void testViewFileContent() throws InterruptedException, FindFailed 
		{
			
			logger = extent.createTest(" test view file content from share tab "); 
			
			globalFunction.viewFileContents("israelFile", false , 0) ;
		}
		
		
		@Test(priority = 16)
		public void testVerifyFileInformationFile() throws InterruptedException {
		
			logger = extent.createTest(" test verify file data from share with me ") ;
			
			globalFunction.verifyFileInformationTab("israelFile" ,"תפוז כתום", "user3","user3pass",false ,0 ) ;
		}
			
		
		@Test(priority = 17)
		public void testMoveFileToFolder() throws InterruptedException, FindFailed 
		{
			logger = extent.createTest("move folder from share tab to folder"); 
			
			globalFunction.MoveFolder_FileToNewLocation("israelFile", "test3", false, 0) ;  
			    
		}
		
		
		@Test(priority = 18)
		public void moveMultipleFilesToFolder() throws InterruptedException {
			
			logger = extent.createTest("move multiple folders from sheared tab to folder");
			
			String[] arrayFiels = {"israelFile", "file_4.txt"} ;  

			globalFunction.MoveMultipleFolder_FileToNewLocation(arrayFiels, "test3", false, 0);

		}
		
		
		@Test(priority = 19)
		public void deleteFile() throws InterruptedException, FindFailed {
			
			logger = extent.createTest("delete file right click");
			
			globalFunction.deleteFolderOrFileFromShearedTab("file_4.txt", 3);

		}
		
		
		@AfterClass
		public void after() 
		{
			extent.flush();
		}	

}
