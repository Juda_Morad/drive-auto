package base;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import globalAction.GlobalFanction;
import globalElements.AllElements;

public class TestBase
{

		public static WebDriver driver;
		public static WebDriver driver1;
		public static ExtentReports extent;
		public static ExtentHtmlReporter htmlReporter;
		public static ExtentTest logger;
		public static WebDriverWait wait;
		public static WebDriverWait wait1;
		public static Properties prop;
		public static Actions action;
		public static Actions action1;
		
		public static void initialization() throws IOException {

			prop = new Properties();  
			FileInputStream ip = new FileInputStream("./configs/properties");
			prop.load(ip);

			System.setProperty("webdriver.chrome.driver", prop.getProperty("chromedriver"));

			driver 		 = new ChromeDriver();
			wait 		 = new WebDriverWait(driver, 30);
			extent 		 = new ExtentReports();
			htmlReporter = new ExtentHtmlReporter("./reports/reports.html");
			extent.attachReporter(htmlReporter);
			action = new Actions(driver);
			
			driver.manage().window().maximize();
			driver.manage().deleteAllCookies();
			driver.navigate().to(prop.getProperty("url"));
		}
		
		public void login() 
		{			
			wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("username"))) ;
			
			driver.findElement(By.id("username")).sendKeys(prop.getProperty("username1"));
			
			driver.findElement(By.id("password")).sendKeys(prop.getProperty("password1")); 
			
			driver.findElement(By.className("btn")).click();	
			
		}
	
}


class beforeWeStart extends TestBase {
	@BeforeSuite
	public void SetUp() throws InterruptedException, IOException {
		
		initialization();	
		
		Thread.sleep(2000) ;
		
		login() ;
		 
		Thread.sleep(2000) ;

	}
	
	
	@AfterSuite
	public void closeChrome() throws InterruptedException, IOException {
		
		driver.quit();
		driver1.quit();

	}
}
	
	
	
	
	
	
	
	
	
	
	
