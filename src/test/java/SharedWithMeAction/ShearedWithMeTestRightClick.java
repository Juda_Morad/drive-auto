
package SharedWithMeAction;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;
import globalElements.AllElements;

public class ShearedWithMeTestRightClick extends TestBase {

	GlobalFanction globalFunction ;
	OperationsForTheOtherDriver operationsForTheOtherDriver ;
	AllElements  allElements ;

	@BeforeClass
	public void setup() throws FindFailed, InterruptedException {

		this.operationsForTheOtherDriver = new OperationsForTheOtherDriver() ;
		this.globalFunction = new GlobalFanction() ;
		this.allElements = new AllElements();
		
		Thread.sleep(2500); 
		allElements.SharedWithMeButton.click();


	}

	// test on folders using right click 

	@Test (priority = 1)
	public void renameRightClickShareWithMe() throws InterruptedException {

		logger = extent.createTest("rename folder from share tab using right click "); 

		globalFunction.renameFoldersAndFiles("test7", "ynet", true , 4);
	}



	@Test(priority = 2)
	public void shareFolderReadPermission() throws InterruptedException {

		logger = extent.createTest(" share folder from sheared tab with read permission using right click"); 

		globalFunction.sharingFileOrFolderWithReadPermission("ynet","תפוז כתום","user3","user3pass",true , 2);
	}


	@Test(priority = 3)
	public void shareFolderEditorPermission() throws InterruptedException {

		logger = extent.createTest(" share folder from sheared tab with editor permission using right click"); 

		globalFunction.sharingFileOrFolderWithReadPermission("ynet","תפוז כתום","user3","user3pass",true , 2);
	}


	@Test(priority = 4)
	public void testRemoveUserPermission() throws InterruptedException {

		logger = extent.createTest(" remove user permission from sheared tab using right click"); 

		globalFunction.removeUserPermission("ynet","תפוז כתום","user3","user3pass", true , 2);
	}


	@Test(priority = 5)
	public void verifyInformationFolder() throws InterruptedException {

		logger = extent.createTest("verify information on folder from sheared tab using right click"); 

		globalFunction.verifyFolderInformationTab("test4", "תפוז כתום", "user3","user3pass",true, true, 0);

	}

	@Test(priority = 6)
	public void moveFolderIntoFolder() throws InterruptedException {

		logger = extent.createTest("move folder into folder from sheared tab right click"); 

		globalFunction.MoveFolder_FileToNewLocation("ynet", "test3", true , 3);

	}


	@Test(priority = 7)
	public void deleteFolder() throws InterruptedException {

		logger = extent.createTest("delete folder from sheared tab right click"); 

		globalFunction.DeleteFolderOrFile("ynet", true, 1);

	}



	//  test on folders using right click 



	@Test(priority = 8)
	public void renameByRightClick() throws InterruptedException, FindFailed {

		logger = extent.createTest("rename file from sheared tab using right click"); 

		Thread.sleep(2000);
		
		globalFunction.renameFoldersAndFiles("israelFile", "yosef456",true, 6);

	}


	@Test(priority = 9)
	public void downloadFileByRightClick() throws InterruptedException, FindFailed {

		logger = extent.createTest("download file right click"); 

		globalFunction.downloadFile("yosef456", true, 2);
		
	}


	@Test(priority = 9)
	public void viewFileContent() throws InterruptedException, FindFailed {

		logger = extent.createTest(" view file content test , using right click ");

		globalFunction.viewFileContents("yosef456", true, 1) ; 

	}


	@Test(priority = 10)
	public void shareFileEditorPermission() throws InterruptedException, FindFailed {

		logger = extent.createTest(" share file with editor permission right click"); 

		globalFunction.sharingFileOrFolderWithEditorPermission("yosef456","תפוז כתום","user3","user3pass", true, 4);

	}

	@Test(priority = 11)
	public void shareFileReadPermission() throws InterruptedException {

		logger = extent.createTest(" share file with read permission right click"); 

		globalFunction.sharingFileOrFolderWithReadPermission("yosef456","תפוז כתום" ,"user3","user3pass",true , 4);
	}

	@Test(priority = 12)
	public void testRemoveUserPermissionOnFile() throws InterruptedException {

		logger = extent.createTest(" remove user permission on file right click"); 

		globalFunction.removeUserPermission("yosef456","תפוז כתום" ,"user3","user3pass", true , 4);
	}

	@Test(priority = 13)
	public void verifyInformationFiles() throws InterruptedException, FindFailed {

		logger = extent.createTest("verify information on files using rigth click");

		globalFunction.verifyFileInformationTab("yosef456","תפוז כתום","user3","user3pass", true, 0) ; 

	}


	@Test(priority = 14)
	public void moveFileIntoFolder() throws InterruptedException {

		logger = extent.createTest("move file into folder right click"); 

		globalFunction.MoveFolder_FileToNewLocation("yosef456", "test4", true , 5);
		
		allElements.MyFilesButton.click();
	}


	@AfterClass
	public void after() 
	{
		extent.flush();
	}

}
