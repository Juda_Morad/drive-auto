package generalTests;

import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalElements.AllElements;

public class GlobalTesting extends TestBase 

{

	AllElements allElements ;
	GlobalFanction globalFanction ;


	@BeforeClass
	public void SetUp() throws InterruptedException, IOException {

		this.allElements = new AllElements();
		this.globalFanction = new GlobalFanction();

	}
	
	
	
	@Test(priority = 1)
	public void  TestContactUs() throws InterruptedException 
	{
		logger = extent.createTest("Test open contact us window ") ;
	
		globalFanction.contactUs(); 
	}
	
	
	@Test(priority = 2)
	public void  TestUsageStorage() throws InterruptedException 
	{
	
		logger = extent.createTest("Test if the usage storage shown is corect ") ;
	
		globalFanction.usageStorage("1.68 KB");   
	}
	
	
	
	@Test(priority = 3)
	public void  TestFilteringFolderOrFileByName() throws InterruptedException 
	{
		
		logger = extent.createTest(" filtering folder or file by name test") ;
		
		String[] FoldersName = {"hither","qaTeam"} ;
		
		globalFanction.filteringFolderOrFileByName(FoldersName);    
	}

	
	
//	@Test(priority = 4)
//	public void  TestviewFoldersInAlphaBetaOrder() throws InterruptedException 
//	{
//		
//		logger = extent.createTest(" filtering folder or file by name test") ;
//		
//		String[] foldersName = {"test","btest","ztest"} ;
//		
//		globalFanction.viewFoldersInAlphaBetaOrder(foldersName) ;
//	}
//	
//	
//	
//	@Test(priority = 5)
//	public void testSharingFileOrFolderWithReadPermission() throws InterruptedException 
//	{
//		
//		logger = extent.createTest(" sharing folder with read permission test") ;
//		
//		globalFanction.sharingFileOrFolderWithReadPermission("yosef","נייטרו הג'לו " ,false , 0);
//	}
//	
//	 
//	
//	
//	
//	@Test(priority = 6)
//	public void testSharingFileOrFolderWithEditorPermission() throws InterruptedException 
//	{
//		
//		logger = extent.createTest(" sharing folder with editor permission test") ;
//		
//		globalFanction.sharingFileOrFolderWithEditorPermission("yosef","נייטרו הג'לו " ,false, 0) ;
//	}
//	
//	
//	
//	@Test(priority = 7)
//	public void testremoveUserPermission() throws InterruptedException 
//	{
//		
//		logger = extent.createTest(" sharing folder with editor permission test") ;
//		
//		globalFanction.removeUserPermission("yosef","נייטרו הג'לו " , false, 0) ;
//	}
//	
//	
//	@Test(priority = 8)
//	public void  testViewFoldersInAlphaBetaOrder() throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest(" filtering folder or file by name test") ;
//		
//		String[] foldersName = {"testb" ,"testa" ,"testz"} ;
//		
//		globalFanction.viewFoldersInAlphaBetaOrder(foldersName);
//		
//		
//	}
//	
//	
//	
//	@Test(priority = 8)
//	public void  testViewFielsBySizeOrder() throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest(" filtering folder or file by size test") ;
//		
//		globalFanction.viewFielsBySizeOrder() ; 
//		
//		
//	}
//	
//	
//	
//	@Test(priority = 9)
//	public void  testViewFoldersByLastChange() throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest(" filtering folder by last change test") ;
//		
//		String[] foldersName = {"test1","test2","test3"} ;
//		
//		globalFanction.viewFoldersByLastChangeOrder(foldersName) ; 	
//		
//	}
//	
//	
//	
//	
//	@Test(priority = 10)
//	public void  testVerifyManePageData() throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest(" verify mane page data  test") ;
//		
//		String[] foldersName = {"test1","test2"} ;
//		
//		globalFanction.verifyManePageData(foldersName); 
//			
//	}
//
//	
//	@Test(priority = 11)
//	public void  testVerifyFileinformation() throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest(" verify file data  test") ;
//		
//		globalFanction.verifyFileInformationTab("yosef.txt") ;
//			
//	}
//	
//	
//	
//	@Test(priority = 12)
//	public void  testVerifyFolderinformation() throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest(" verify mane page data  test") ;
//		
//		globalFanction.verifyFolderInformationTab("test1") ;
//			
//	}
//	
//	
//	@Test(priority = 13)
//	public void  testVerifyMultipleFolderAndFileInformationTab() throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest(" verify mane page data  test") ;
//		
//		String[] folderAndFileName = {"test1" , "file_4.txt" , "yosef.txt" };
//		
//		globalFanction.verifyMultipleFolderAndFileInformationTab(folderAndFileName);
//			
//	}
//	
//	
//	@Test(priority = 14)
//	public void testMoveFileOrFolderToShareWithMeFolder () throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest("test move file or folder to share with me folder") ;
//		
//		globalFanction.CreateFolder("test3");
//		
//		globalFanction.moveFileOrFolderToShareWithMeFolder("test3","ariel") ;
//			
//	}
//	
//	
//	
//
//	@Test(priority = 15)
//	public void testViewFileContents () throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest("test view file contents") ;
//		
//		globalFanction.viewFileContents("Search.png");
//			
//	}
//	
//	
//	
//
//	@Test(priority = 16)
//	public void testVerifyGeneralDataOnFolder () throws InterruptedException, FindFailed 
//	{
//		 
//		logger = extent.createTest("test verify general data on folder") ;
//		
//		String[] foldersName = {"test1" , "test2"} ;
//		
//		globalFanction.verifyGeneralDataOnFolder("test5", foldersName);
//			
//	}
//	

	
	@AfterClass
	public void after() 
	{
		extent.flush();
	}
	
}
