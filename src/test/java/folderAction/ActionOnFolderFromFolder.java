package folderAction;

import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;
import globalElements.AllElements;

public class ActionOnFolderFromFolder extends TestBase
{


	AllElements allElements ;
	GlobalFanction globalFanction ;
	OperationsForTheOtherDriver operationsForTheOtherDriver ;
	
	

	@BeforeClass
	public void SetUp() throws InterruptedException, IOException {

		this.allElements = new AllElements();
		this.globalFanction = new GlobalFanction();
		operationsForTheOtherDriver = new OperationsForTheOtherDriver() ;
	}
	
	@Test(priority = 1) 
	public void TestcreateFolder() throws InterruptedException  
	{
		logger = extent.createTest("Test create Folder from folder") ;	
		
		globalFanction.CreateFolder("israel"); 
		
		globalFanction.EnterToFolder("israel"); 
		
		globalFanction.CreateFolder("yosefariel");
		
		
	}
	
	
	
	@Test(priority = 2)
	public void testSharingFileOrFolderWithReadPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" sharing folder from folder with read permission test") ;
		
		globalFanction.sharingFileOrFolderWithReadPermission("yosefariel","אחמד אדידס" ,"user2","user2pass" ,false , 0);
	
	}
	
	 
	
	
	
	@Test(priority = 3)
	public void testSharingFileOrFolderWithEditorPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" sharing folder from folder with editor permission test") ;
		
		globalFanction.sharingFileOrFolderWithEditorPermission("yosefariel","אחמד אדידס","user2","user2pass" ,false, 0) ;
		
	}
	
	
	
	

	@Test(priority = 4)
	public void testremoveUserPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" sharing folder from folder with editor permission test") ;
		
		globalFanction.removeUserPermission("yosefariel","אחמד אדידס" ,"user2","user2pass", false, 0) ;
		
	}
	
	
	
	
	
	@Test(priority = 5)
	public void TestRenameFolders() throws InterruptedException 
	
	{
		logger = extent.createTest("rename Folders from folder") ;
		
		globalFanction.renameFoldersAndFiles("yosefariel","arielyosef", false , 0);  
		
		globalFanction.DeleteFolderOrFile("arielyosef", false , 0);
	}
	
	
	@Test(priority = 6) 
	public void moveFolderToHomePage() throws InterruptedException 
	{
	    logger = extent.createTest("move folder from folder to mane page") ;
	
		globalFanction.CreateFolder("arielyosef");
	    
		globalFanction.MoveFolder_FileToMainPage("israel", "arielyosef", false , 0); 
		
	}
	
	@Test(priority = 7) 
	public void moveMultipleFoldersToManePage() throws InterruptedException 
	{
	    logger = extent.createTest("move multiple folders from folder to mane page") ;
	    
	    String[] arrayFiels = {"yosefFolder9", "yosefFolder10"}; 
	    
	    globalFanction.CreateFolder("yosefFolder9");
		globalFanction.CreateFolder("yosefFolder10");
	    
		globalFanction.MoveMultipleFolder_FileToMainPage("israel",arrayFiels, false ,0); 
		
	}
	 
	@Test(priority = 8)
	public void switchViewTest() throws InterruptedException 
	{

		logger = extent.createTest("switchViewTest") ;
		
		globalFanction.CreateFolder("test7");
 
		globalFanction.SwitchView();
		
	} 
	
	@Test(priority = 9) 
	public void deleteMultipleFolders() throws InterruptedException  
	{
		logger = extent.createTest("delete multiple folders from folder") ;
		
		String[] arrayFiels = {"yosefTest5" ,"yosefTest6" ,"test7"};
		
		globalFanction.CreateFolder("yosefTest5");
		globalFanction.CreateFolder("yosefTest6");
	
		globalFanction.DeleteMultipleFolderOrFile(arrayFiels, false , 0) ; 
		
	}
	
	
	
	
	@Test(priority = 10)
	public void  testViewFoldersInAlphaBetaOrder() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" filtering folders from folder by name test") ;
		
		String[] foldersName = {"testb" ,"testa" ,"testz"} ;
		
		globalFanction.viewFoldersInAlphaBetaOrder(foldersName);
		
		
	}
	
	
	
	@Test(priority = 11)
	public void  testViewFoldersByLastChange() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" filtering folder from folderby last change test") ;
		
		String[] foldersName = {"test1","test2","test3"} ;
		
		globalFanction.viewFoldersByLastChangeOrder(foldersName) ; 
		
		
	}
	
	
	
	
	@Test(priority = 12)
	public void  testVerifyFolderinformation() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" verify folder from folder data  test") ;
		 
		globalFanction.verifyFolderInformationTab("test1","אחמד אדידס","user2","user2pass",false,false ,0 ) ;
	}
	

	
	@Test(priority = 13)
	public void testMoveFolderToShareWithMeFolder () throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest("test move folder from folder to share with me folder") ;
		
		operationsForTheOtherDriver.loginToOtherUser("user2","user2pass");
		
		operationsForTheOtherDriver.CreateFolder("test6");  
		
		operationsForTheOtherDriver.shareFileOrFolder("test6", "נייקי אדידס");
		
		driver1.quit();
		
		Thread.sleep(1500) ; 
		
		globalFanction.moveFileOrFolderToShareWithMeFolder("test1","test6") ;
		
		globalFanction.EnterToFolder("israel");  
			
	}
	


	@Test(priority = 14)
	public void testVerifyGeneralDataOnFolder () throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest("test verify general data on folder") ;
		
		String[] foldersName = {"test1" , "test2"} ;
		
		globalFanction.verifyGeneralDataOnFolder("israel", foldersName);
		
		allElements.MyFilesButton.click();  
		
		globalFanction.DeleteFolderOrFile("israel", false, 0) ; 
			
	}
	
	
	
	
	
	
	@AfterClass
	public void after() 
	{
		extent.flush();
	}
	
}
