package globalAction;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Pattern;
import org.sikuli.script.Screen;

import com.aventstack.extentreports.Status;

import base.TestBase;
import globalElements.AllElement1;
import globalElements.AllElements;

public class OperationsForTheOtherDriver extends TestBase

{	

	AllElement1 allElement1 ;



	// login to other user function 
	public void loginToOtherUser(String userName ,String userPass ) throws InterruptedException 
	{

		ChromeOptions options = new ChromeOptions();
		options.addArguments("--incognito");
		DesiredCapabilities capabilities = DesiredCapabilities.chrome();
		capabilities.setCapability(ChromeOptions.CAPABILITY, options);

		driver1     = new ChromeDriver() ;		
		allElement1 = new AllElement1() ; 
		action1     = new Actions(driver1) ;
		wait1        = new WebDriverWait(driver1, 30);

		driver1.manage().window().maximize();
		driver1.manage().deleteAllCookies();
		driver1.navigate().to(prop.getProperty("url"));		

		wait1.until(ExpectedConditions.visibilityOfElementLocated(By.id("username"))) ;
		
		driver1.findElement(By.id("username")).sendKeys(userName);

		driver1.findElement(By.id("password")).sendKeys(userPass); 

		Thread.sleep(1000) ; 

		driver1.findElement(By.className("btn")).click();	

		Thread.sleep(2000); 


	}



	// test if a folder has been created 

	public void CreateFolder(String name) throws InterruptedException  
	{
		int numOfFolders1 = allElement1.NumOfItem.size() ;

		allElement1.NewFolderButton.click();

		allElement1.InputField.sendKeys(name); 

		wait1.until(ExpectedConditions.visibilityOf(allElement1.CreateFolderConfirmButton));

		allElement1.CreateFolderConfirmButton.click(); 

		Thread.sleep(3000);
		int numOfFolders2 = allElement1.NumOfItem.size() ;

		if (numOfFolders2 > numOfFolders1)  
		{
			logger.log(Status.PASS , "The folder was created and added to the folder list");

		}else  
		{

			logger.log(Status.FAIL ,"The folder was not created");

		}
		
		Thread.sleep(1500) ; 

	}


	// Find a folder or file by its name
	public WebElement LocateFolder_fileByName(String name) throws InterruptedException

	{
		Thread.sleep(2000);
		WebElement element = null;

		for(int i = 0 ; i < allElement1.NumOfItem.size() ; i++ ) 
		{
			if(allElement1.NumOfItem.get(i).getText().equals(name)) 
			{
				element  = allElement1.NumOfItem.get(i) ;
				break ;

			}

		}
		return element  ;  	
	}




	// Open folder operation
	public void EnterToFolder(String name) throws InterruptedException 
	{

		Thread.sleep(1000) ; 

		WebElement element ;

		element = LocateFolder_fileByName(name) ;

		if(element == null) 
		{
			logger.log(Status.FAIL ,"existing folder not opens");		

		}else 
		{

			action1.doubleClick(element).perform();

			logger.log(Status.PASS , "existing folder opens");

		}

	}




	// File or folder sharing with read permission operation
	//		public void sharingFileOrFolderWithReadPermission(String file_folder_name ,String nameOfTheUser , boolean right , int number ) throws InterruptedException 
	//		{
	//			Thread.sleep(2000);  
	//
	//			LocateFolder_fileByName(file_folder_name).click();
	//
	//			if (right) {
	//
	//				rightClick(number , file_folder_name);	
	//			}
	//			else {
	//
	//				WaitForVisibility(allElement1.ShareButton) ; 
	//
	//				allElement1.ShareButton.click();
	//			}
	//
	//			WaitForVisibility(allElement1.searchUserFild) ; 
	//
	//			allElement1.searchUserFild.click();
	//
	//			allElement1.searchUserFild.sendKeys(nameOfTheUser) ;
	//
	//			Thread.sleep(1500) ; 
	//
	//			action.sendKeys(Keys.ENTER).build().perform();
	//
	//			Select select = new Select(allElement1.selectPermissionButton) ;
	//
	//			select.selectByIndex(0); 
	//
	//			Thread.sleep(2000) ; 
	//
	//			WaitForVisibility(allElement1.CreateButtonForShare) ;
	//
	//			allElement1.CreateButtonForShare.click();
	//
	//			Thread.sleep(1000) ;
	//
	//			allElement1.closeShareButton.click();
	//
	//			// login to other user  
	//
	//			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	//
	//			loginToOtherUser("nitro","jello");
	//			
	//			AllElement1	allElement1 = new AllElement1() ;
	//			
	//			allElement1.SharedWithMeButton.click();
	//			
	//			// driver1.findElement(By.cssSelector("[aria-label='שותפו איתי'].action")).click();
	//
	//			if(operationsForTheOtherDriver.LocateFolder_fileByName(file_folder_name)!= null) {
	//
	//				operationsForTheOtherDriver.LocateFolder_fileByName(file_folder_name).click(); 
	//
	//				logger.log(Status.PASS ,"file / folder successfully shared ");		
	//			}else {
	//
	//				logger.log(Status.FAIL ," sharing file / folder operation faield ");		
	//			}
	//
	//			Thread.sleep(2000) ; 
	//
	//			WebElement shareButton = driver1.findElement(By.cssSelector("[aria-label='שיתוף']")) ;
	//
	//			String shareButtonText = shareButton.getCssValue("display") ;
	//
	//			Thread.sleep(1500) ; 
	//
	//			if(shareButtonText.equals("none")) {
	//
	//				logger.log(Status.PASS ,"file / folder successfully shared with corect permission ");	
	//
	//			}else {
	//
	//				logger.log(Status.FAIL ,"share button is display .. Despite read-only permission");	
	//			}	
	//
	//
	//			driver1.quit() ;
	//
	//			driver.switchTo().window(tabs.get(0));	
	//
	//			LocateFolder_fileByName(file_folder_name).click();
	//
	//		}


	// File or folder sharing with editor permission operation
	//		public void sharingFileOrFolderWithEditorPermission(String file_folder_name ,String nameOfTheUser , boolean right , int number ) throws InterruptedException 
	//		{
	//			Thread.sleep(2000);  
	//
	//			LocateFolder_fileByName(file_folder_name).click();
	//
	//			if (right) {
	//
	//				rightClick(number , file_folder_name);	
	//			}
	//			else {
	//
	//				WaitForVisibility(allElement1.ShareButton) ; 
	//
	//				allElement1.ShareButton.click();
	//			}
	//
	//			WaitForVisibility(allElement1.searchUserFild) ; 
	//
	//			allElement1.searchUserFild.click();
	//
	//			allElement1.searchUserFild.sendKeys(nameOfTheUser) ;
	//
	//			Thread.sleep(1500) ; 
	//
	//			action.sendKeys(Keys.ENTER).build().perform();
	//
	//			Select select = new Select(allElement1.selectPermissionButton) ;
	//
	//			select.selectByIndex(1); 
	//
	//			Thread.sleep(2000) ; 
	//
	//			WaitForVisibility(allElement1.CreateButtonForShare) ;
	//
	//			allElement1.CreateButtonForShare.click();
	//
	//			Thread.sleep(1000) ;
	//
	//			allElement1.closeShareButton.click();
	//
	//			// login to other user  
	//
	//			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	//
	//			operationsForTheOtherDriver.loginToOtherUser("nitro","jello");
	//
	//			driver1.findElement(By.cssSelector("[aria-label='שותפו איתי'].action")).click();
	//
	//			if(operationsForTheOtherDriver.LocateFolder_fileByName(file_folder_name)!= null) {
	//
	//				operationsForTheOtherDriver.LocateFolder_fileByName(file_folder_name).click(); 
	//
	//				logger.log(Status.PASS ,"file / folder successfully shared ");		
	//			}else {
	//
	//				logger.log(Status.FAIL ," sharing file / folder operation faield ");		
	//			}
	//
	//			Thread.sleep(2000) ; 
	//
	//			WebElement shareButton = driver1.findElement(By.cssSelector("[aria-label='שיתוף']")) ;
	//
	//			try {
	//				shareButton.click();
	//
	//				Thread.sleep(1500) ; 
	//
	//				WebElement deletePermissionButton = driver1.findElement(By.className("delete-permission")) ;
	//
	//				if(deletePermissionButton.isDisplayed()) {
	//
	//					logger.log(Status.PASS ,"file / folder successfully shared with corect permission ");	
	//
	//				}
	//
	//			} catch (Exception e) {
	//
	//				logger.log(Status.FAIL ,"share button is not work even the permissin is \"editor\" ");	
	//			}
	//
	//			driver1.quit() ;
	//
	//			driver.switchTo().window(tabs.get(0));	
	//
	//			LocateFolder_fileByName(file_folder_name).click();
	//
	//		}





	// Remove user permission function
	//		public void removeUserPermission(String file_folder_name ,String nameOfTheUser , boolean right , int number) throws InterruptedException 
	//		{
	//
	//			Thread.sleep(2000);  
	//
	//			LocateFolder_fileByName(file_folder_name).click();
	//
	//			if (right) {
	//
	//				rightClick(number , file_folder_name);	
	//			}
	//			else {
	//
	//				WaitForVisibility(allElement1.ShareButton) ; 
	//
	//				allElement1.ShareButton.click();
	//			}
	//
	//			WaitForVisibility(allElement1.deletePermissionButton);
	//
	//			allElement1.deletePermissionButton.click();
	//
	//			allElement1.closeShareButton.click();
	//
	//			// login to other user  
	//
	//			ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
	//
	//			operationsForTheOtherDriver.loginToOtherUser("nitro","jello");
	//
	//			driver1.findElement(By.cssSelector("[aria-label='שותפו איתי'].action")).click();
	//
	//			if(operationsForTheOtherDriver.LocateFolder_fileByName(file_folder_name)== null) {
	//
	//				logger.log(Status.PASS ,"Remove user permission action done Successfully ");	
	//
	//			}else {
	//
	//				logger.log(Status.FAIL ,"Remove user permission action faield ") ; 	
	//			}
	//
	//			driver1.quit() ;
	//
	//			driver.switchTo().window(tabs.get(0)); 
	//
	//			LocateFolder_fileByName(file_folder_name).click();
	//
	//		} 




	// Create a new name for a folder or file 
	public void renameFoldersAndFiles(String name ,String reName , boolean right , int number) throws InterruptedException 	
	{


		LocateFolder_fileByName(name).click() ;

		Thread.sleep(1000);

		if (right) {

			rightClick(number, name);	
		}
		else {

			allElement1.ReNameButton.click(); 
		}
		try {

			Thread.sleep(1000);

			if (allElement1.windowOfToolBar.isDisplayed()) {

				try {

					clear1() ;

					allElement1.InputField.sendKeys(reName);

					WaitForVisibility(allElement1.RenameConfirmButton); 

					allElement1.RenameConfirmButton.click();

					Thread.sleep(1000);

					if (allElement1.windowOfToolBar.isDisplayed()) {

						allElement1.CloseButton.click();

						LocateFolder_fileByName(name).click() ;

						logger.log(Status.FAIL, "rename buttun not work");
					}

				} catch (Exception e) {

					logger.log(Status.PASS, "rename buttun work");

				}
			}

		} catch (Exception e) {

			logger.log(Status.FAIL, "get into rename not work");

		} finally {

			if(reName.equals(LocateFolder_fileByName(reName).getText())) 
			{

				logger.log(Status.PASS , "The name was changed successfully");	
			}

			else  
			{
				logger.log(Status.FAIL , "The name was not changed successfully");	
			}

		}
	}

	// Test move a file or folder from the home screen to another folder
	public void MoveFolder_FileToNewLocation(String fromFolder , String toFolder , boolean right , int number) throws InterruptedException  
	{

		WebElement folderTo =null ;

		Thread.sleep(2000); 

		CreateFolder(toFolder); 

		LocateFolder_fileByName(fromFolder).click() ; 

		try {

			if(right) {

				rightClick(number ,fromFolder);	
			}
			else {

				allElement1.MoveButton.click();
			}

			Thread.sleep(1000);

			if (allElement1.windowOfToolBar.isDisplayed()) {

				try {
					Thread.sleep(2000); 
					for(int i = 0 ; i < allElement1.folderListMoveButton.size() ; i++) 
					{
						if(allElement1.folderListMoveButton.get(i).getText().equals(toFolder)) 
						{
							folderTo = allElement1.folderListMoveButton.get(i) ;
							break ;
						}

					}

					folderTo.click(); 

					WaitForVisibility(allElement1.MoveConfirmButton); 

					allElement1.MoveConfirmButton.click();

					Thread.sleep(1000); 

					Thread.sleep(1000);

					if (allElement1.windowOfToolBar.isDisplayed()) {

						allElement1.CloseButton.click();

						logger.log(Status.FAIL, "move buttun not work");
					}

				} catch (Exception e) {

					logger.log(Status.PASS, "move buttun work");

				}
			}

		} catch (Exception e) {

			logger.log(Status.FAIL, "get into move folder not work");

		} finally {

			if(fromFolder.equals(LocateFolder_fileByName(fromFolder).getText()))
			{
				logger.log(Status.PASS , "Folder successfully moved");
			}

			else

			{
				logger.log(Status.FAIL , "Folder not moved successfully");

			}

			allElement1.homeButtonIcon.click();

		}

	}

	//   Move files or folders from the home screen to another folder
	public void MoveMultipleFolder_FileToNewLocation(String[] nameFolderArray, String toFolder , boolean right , int number) throws InterruptedException  
	{
		int exsist = 0 ;

		WebElement folderTo = null ;		

		Thread.sleep(2000);
		multipleSelect(nameFolderArray); 

		try {

			if (right) {

				rightClick(number , null);	
			}
			else {

				Thread.sleep(2000);
				allElement1.MoveButton.click();
			}

			Thread.sleep(1500);

			if (allElement1.windowOfToolBar.isDisplayed()) {

				try {

					for(int i = 0 ; i < allElement1.folderListMoveButton.size() ; i++) 
					{
						if(allElement1.folderListMoveButton.get(i).getText().equals(toFolder)) 
						{
							folderTo = allElement1.folderListMoveButton.get(i) ;
							break ;
						}
					}

					Thread.sleep(2000);
					folderTo.click(); 	

					WaitForVisibility(allElement1.MoveConfirmButton); 

					allElement1.MoveConfirmButton.click();

					Thread.sleep(1500);

					if (allElement1.windowOfToolBar.isDisplayed()) {

						allElement1.CloseButton.click();

						logger.log(Status.FAIL, "move buttun not work");
					}

				} catch (Exception e) {

					logger.log(Status.PASS, "move buttun work");
				}
			}	

		} catch (Exception e) {

			logger.log(Status.FAIL, "get into move folder not work");

		} finally {

			for(String folder : nameFolderArray) {

				if(folder.equals(LocateFolder_fileByName(folder).getText())) {

					exsist ++ ;
				}
			}

			if(exsist == nameFolderArray.length) {

				logger.log(Status.PASS , "Folder successfully moved"); 
			}else {

				logger.log(Status.FAIL , "Folder not moved successfully");
			}


			driver.navigate().refresh();

			Thread.sleep(1000);

			allElement1.homeButtonIcon.click();

		}
	}


	// Move a file or folder from a folder to the home screen
	public void MoveFolder_FileToMainPage(String folder , String folder_file , boolean right , int number) throws InterruptedException  
	{

		LocateFolder_fileByName(folder_file).click(); 

		if (right) {

			rightClick(number, folder_file);
		}
		else {

			allElement1.MoveButton.click();
		}

		WaitForVisibility(allElement1.navigateBackInFolders); 

		allElement1.navigateBackInFolders.click();

		WaitForVisibility(allElement1.MoveConfirmButton); 

		allElement1.MoveConfirmButton.click();

		Thread.sleep(2000) ;

		driver.navigate().refresh();

		Thread.sleep(2000) ;

		if(folder_file.equals(LocateFolder_fileByName(folder_file).getText())) 
		{
			logger.log(Status.PASS , "Folder successfully moved to main page");

			DeleteFolderOrFile(folder_file ,false , 0); 

		}else
		{
			logger.log(Status.FAIL , "Folder not  moved successfully to main page");
		}

		Thread.sleep(1000); 

		EnterToFolder(folder);  


	}


	//  Transfer files or a folder from a folder to the home screen
	public void MoveMultipleFolder_FileToMainPage(String folder ,String[] nameFolderArray , boolean right , int number) throws InterruptedException  
	{

		int exsist = 0 ;

		Thread.sleep(1500) ; 

		multipleSelect(nameFolderArray);

		if (right) {

			rightClick(number, null);
		}
		else {
			allElement1.MoveButton.click();
		}

		WaitForVisibility(allElement1.navigateBackInFolders) ;  

		allElement1.navigateBackInFolders.click();

		WaitForVisibility(allElement1.MoveConfirmButton); 

		allElement1.MoveConfirmButton.click();

		Thread.sleep(2000) ;

		driver.navigate().refresh();

		Thread.sleep(2000) ;

		for(String folderText : nameFolderArray) {

			if(folderText.equals(LocateFolder_fileByName(folderText).getText())) {

				exsist ++ ;
			} 
		}

		if(exsist == nameFolderArray.length) {

			logger.log(Status.PASS , "all Folders successfully moved to main page");

			DeleteMultipleFolderOrFile(nameFolderArray , false , 0); 

		}else {

			logger.log(Status.FAIL , "Folders not  moved successfully to main page");
		}

		Thread.sleep(2000); 

		EnterToFolder(folder);  


	}


	// Move files or folders from one folder to another that exists before in the file archive
	public void MoveMultipleFolder_FileToPreviousFolder(String[] nameFolderArray, String secondFolder , boolean right , int number) throws InterruptedException 
	{

		returnsFolderFromHierarchyList(secondFolder).click();  

		Thread.sleep(2000) ; 

		int exsist = 0 ;

		multipleSelect(nameFolderArray) ;

		if (right) {

			rightClick(number, null);
		}
		else {

			allElement1.MoveButton.click(); 
		}

		Thread.sleep(2000) ; 

		action.doubleClick(allElement1.navigateBackInFolders).build().perform();

		Thread.sleep(10000) ; 

		WaitForVisibility(allElement1.MoveConfirmButton); 

		allElement1.MoveConfirmButton.click() ;

		Thread.sleep(1000); 

		returnsFolderFromHierarchyList("israel").click();  

		Thread.sleep(3000); 

		for(String folderText : nameFolderArray) {

			if(folderText.equals(LocateFolder_fileByName(folderText).getText())) {

				exsist ++ ;
			}

		}

		if(exsist == nameFolderArray.length) {

			logger.log(Status.PASS , "Folder successfully moved beck");

			DeleteMultipleFolderOrFile(nameFolderArray , false , 0);

			DeleteFolderOrFile(secondFolder , false , 0); 
		}else {

			logger.log(Status.FAIL , "Folders moved beck in folders action failed");
		}
	}


	//  Delete a file or folder
	public void DeleteFolderOrFile(String name , boolean right , int number ) throws InterruptedException  
	{
		Thread.sleep(2000);

		int numOfItem = allElement1.NumOfItem.size() ;

		LocateFolder_fileByName(name).click() ; 

		if (right) {

			rightClick(number, name);
		} 
		else {

			allElement1.DeleteButton.click();
		}

		WaitForVisibility(allElement1.DeleteConfirmButton);

		allElement1.DeleteConfirmButton.click() ;        

		Thread.sleep(3000); 

		int numOfItem2 = allElement1.NumOfItem.size() ;

		if(numOfItem2 < numOfItem) 
		{
			logger.log(Status.PASS , "Folder/file successfully deleted");
		}else 
		{
			logger.log(Status.FAIL , "Folder/file not deleted successfully");
		}

	}

	//   Delete multiple files or folders
	public void DeleteMultipleFolderOrFile(String[] nameFolderArray , boolean right , int number) throws InterruptedException  
	{

		Thread.sleep(1000) ; 

		int numOfItem = allElement1.NumOfItem.size() ;

		Thread.sleep(2000);

		multipleSelect(nameFolderArray);  

		try {
			if (right) {

				rightClick(number, null);
			}
			else {
				allElement1.DeleteButton.click();
			}

			if (allElement1.windowOfToolBarDelete.isDisplayed()) {

				try {

					allElement1.DeleteConfirmButton.click() ;        

					Thread.sleep(1500);

					if (allElement1.windowOfToolBarDelete.isDisplayed()) {

						allElement1.CloseButton.click();

						multipleSelect(nameFolderArray) ;

						logger.log(Status.FAIL, "delete button not work");
					}

				} catch (Exception e) {	

					logger.log(Status.PASS, "delete button work");
				}
			}

		} catch (Exception e) {

			logger.log(Status.FAIL, "get into delete");

		} finally {

			Thread.sleep(1500) ; 

			int numOfItem2 = allElement1.NumOfItem.size() ;

			if(numOfItem2 + nameFolderArray.length == numOfItem) 
			{
				logger.log(Status.PASS , "Folder/file successfully deleted");
			}

			else 

			{
				logger.log(Status.FAIL , "Folder/file not deleted successfully");

			}
		}

	}

	// Function that checks that when a view change is selected, the view really changes 
	public void SwitchView() throws InterruptedException 
	{
		WaitForVisibility(allElement1.SwitchViewButton) ;

		allElement1.SwitchViewButton.click();

		try {

			if(allElement1.SwitchViewBlock.isDisplayed()) 
			{
				logger.log(Status.PASS , "switch view works successfully"); 
			}
		} catch (Exception e) {

			logger.log(Status.FAIL , "switch view action failed"); 

		}finally {

			allElement1.SwitchViewButton.click();
		}

	}



	//  Check that when the file is downloaded the file is actually in the folder on your computer
	public void downloadFile(String nameOfFile , boolean right , int number) throws InterruptedException 
	{
		String fileDirectory = "/home/yosef/Downloads/"+ nameOfFile  ;

		File downloadFile = new File(fileDirectory) ;

		LocateFolder_fileByName(nameOfFile).click();

		if (right) {

			rightClick(number, nameOfFile);
		}
		else {

			WaitForVisibility(allElement1.DownloadButton) ;

			allElement1.DownloadButton.click();
		}

		Thread.sleep(5000);

		if(downloadFile.exists())			
		{
			logger.log(Status.PASS , "File downloaded successfully");

		}else 
		{
			logger.log(Status.FAIL ,"File not downloaded successfully") ;
		}

		Thread.sleep(1000);
		downloadFile.delete() ;

		Thread.sleep(1000) ; 

		LocateFolder_fileByName(nameOfFile).click() ;

	} 



	// Multiple selection of folders and files
	public void multipleSelect(String[] nameFolderArray) throws InterruptedException 
	{ 

		while (allElement1.multipleSelection.size() != 1) {

			allElement1.SelectMultipleButton.click();
		}

		for(String folder : nameFolderArray) {

			LocateFolder_fileByName(folder).click(); 
		}
	}


	// Log in to the files tab shared with me
	public void LogInToSharedWithMeTab() 
	{

		allElement1.SharedWithMeButton.click();

	}


	// Checking the Contact  window  opens when contact us button was selected 
	public void contactUs() throws InterruptedException 
	{
		String pageUrl = "https://open.rocket.chat/home" ; 

		String parentHandle = driver.getWindowHandle();

		allElement1.ContactUsButton.click();

		Thread.sleep(10000);

		for (String winHandle : driver.getWindowHandles()) {   
			driver.switchTo().window(winHandle);   
		}  

		String currentUrl = driver.getCurrentUrl() ;  

		if(pageUrl.equals(currentUrl))
		{

			logger.log(Status.PASS , "A contact window opens") ;
		}else 
		{
			logger.log(Status.FAIL , "A contact window not opens") ; 
		}

		Thread.sleep(2000);
		driver.close(); 

		Thread.sleep(2000);
		driver.switchTo().window(parentHandle);
	} 


	//   function to upload  file using linux   
	public void uploadFile(String pngPattern , String nameOfFile) throws InterruptedException, FindFailed
	{
		Thread.sleep(2000); 

		WaitForVisibility(allElement1.uploadFilebutton); 

		allElement1.uploadFilebutton.click();

		Thread.sleep(2000);  

		Screen screen = new Screen() ; 

		Pattern file = new Pattern(prop.getProperty("path")+pngPattern) ;

		Pattern openButton = new Pattern(prop.getProperty("path")+"openButton.png") ;

		Pattern SearchButton = new Pattern(prop.getProperty("path")+"Search.png") ;

		Pattern searchField = new Pattern(prop.getProperty("path")+"searchfield.png") ;  

		Thread.sleep(1000); 

		screen.find(SearchButton) ;
		Thread.sleep(1000); 

		screen.click(SearchButton) ;	

		screen.find(searchField) ;
		screen.type(searchField,nameOfFile) ;

		Thread.sleep(2000); 
		screen.find(file) ;

		Thread.sleep(2000); 
		screen.click(file) ;

		Thread.sleep(1000); 
		screen.find(openButton) ;

		Thread.sleep(1000); 
		screen.click(openButton) ;

		Thread.sleep(2000); 


		if(nameOfFile.equals(LocateFolder_fileByName(nameOfFile).getText())) 

		{
			logger.log(Status.PASS , " file upload successfully ") ;

		}else 
		{
			logger.log(Status.FAIL , " file not upload successfully ") ;
		}

	} 


	// upload  a file to a drive by dragging the file used by Linux 
	public void upLoadFile_dragAndDrop(String pngPattern ,String pngPatternAfterClick , String nameOfFile) throws InterruptedException, FindFailed 
	{
		allElement1.uploadFilebutton.click();

		Thread.sleep(2000); 

		Screen screen = new Screen() ;

		Pattern file= new Pattern(prop.getProperty("path")+pngPattern) ; 

		Pattern SearchButton = new Pattern(prop.getProperty("path")+"Search.png") ;

		Pattern searchField = new Pattern(prop.getProperty("path")+"searchfield.png") ;

		Pattern cancelButton = new Pattern(prop.getProperty("path")+"cancelButton.png") ;

		Pattern pastPlace = new Pattern(prop.getProperty("path")+"pastPlace.png") ; 

		Pattern fileAfterClick= new Pattern(prop.getProperty("path")+pngPatternAfterClick) ;

		Thread.sleep(1000); 

		screen.find(SearchButton) ;
		Thread.sleep(1000); 

		screen.click(SearchButton) ;	
		Thread.sleep(1000);

		screen.find(searchField) ;
		Thread.sleep(1000);

		screen.type(searchField,nameOfFile) ;	

		Thread.sleep(1000); 
		screen.find(file) ;

		screen.click(file) ;
		Thread.sleep(1000); 

		screen.dragDrop(fileAfterClick,pastPlace) ;

		Thread.sleep(1000);

		screen.click(cancelButton) ; 

		if(nameOfFile.equals(LocateFolder_fileByName(nameOfFile).getText())) 
		{
			logger.log(Status.PASS , " file upload successfully ") ;

		}else 
		{ 
			logger.log(Status.FAIL , " file not upload successfully ") ;
		}


	}



	//   Checking the correct amount of storage used
	public void usageStorage(String ExpectedStorageVolume) throws InterruptedException  
	{
		String ExpectedStorageVolumeText = "Current usage: " + ExpectedStorageVolume + " / 10 GB" ;

		String ActualStorageVolumeText = allElement1.currentUsage.getText() ;

		Thread.sleep(3000);

		if(ExpectedStorageVolumeText.equals(ActualStorageVolumeText)) 
		{

			logger.log(Status.PASS , "the current usage value is correct ") ;

		}else 
		{
			logger.log(Status.FAIL , "the current usage value is not correct ") ; 

		}

	}




	// Filtering a folder or file by name
	public void filteringFolderOrFileByName() throws InterruptedException
	{

		CreateFolder("test") ; 

		CreateFolder("rename") ; 

		allElement1.searchInputField.click();

		allElement1.searchInputField.sendKeys("te");

		Thread.sleep(3000) ; 

		WebElement dropdown = driver.findElement(By.id("autocomplete-result-list-1"));

		List<WebElement> options = dropdown.findElements(By.tagName("li"));

		if(options.size() == 1) {

			logger.log(Status.PASS , "filtering folder or file by name ") ;

		}else {

			logger.log(Status.FAIL , "the current usage value is correct ") ;
		}

		Thread.sleep(1000) ; 

		clear1() ; 
	}



	// view folders in alpha beta order 
	public void viewFoldersInAlphaBetaOrder(String[] foldersName) throws InterruptedException 
	{	

		for(int i = 0 ; i < foldersName.length ; i++) {

			CreateFolder(foldersName[i]) ;  

		}

		Thread.sleep(1000) ; 

		String[] folderListUfterClicking = new String[allElement1.foldersName.size()] ;

		String[] sortsFoldersList = new String[allElement1.foldersName.size()] ;

		WaitForVisibility(allElement1.sortByName) ;

		allElement1.sortByName.click() ; 

		allElement1.sortByName.click() ; 

		Thread.sleep(1500) ; 

		for(int i = 0 ; i < allElement1.foldersName.size() ; i++) {

			folderListUfterClicking[i] = allElement1.foldersName.get(i).getText() ;

			sortsFoldersList[i] = allElement1.foldersName.get(i).getText() ;
		}

		Arrays.sort(sortsFoldersList);

		if(Arrays.equals(sortsFoldersList, folderListUfterClicking)) { 

			logger.log(Status.PASS , " folder sorting by text action work Successfully ") ;

		}else {

			logger.log(Status.FAIL , "folder sorting by text action failed ") ;
		}

		DeleteMultipleFolderOrFile(foldersName, false , 0);

	}




	// view fiels in alpha beta order 
	public void viewFielsInAlphaBetaOrder() throws FindFailed, InterruptedException 
	{

		uploadFile("file_1.png","yosef.txt") ; 

		uploadFile("file_4.png" ,"file_4.txt") ; 

		String[] fielsListUfterClicking = new String[allElement1.fielsNameList.size()] ;

		String[] sortsFielsList = new String[allElement1.fielsNameList.size()] ;

		WaitForVisibility(allElement1.sortByName) ;

		allElement1.sortByName.click() ; 
		allElement1.sortByName.click() ; 

		Thread.sleep(1500) ;  

		for(int i = 0 ; i < allElement1.fielsNameList.size() ; i++) {

			fielsListUfterClicking[i] = allElement1.fielsNameList.get(i).getText() ;

			sortsFielsList[i] = allElement1.fielsNameList.get(i).getText() ;
		}

		Arrays.sort(sortsFielsList);

		if(Arrays.equals(sortsFielsList, fielsListUfterClicking)) { 

			logger.log(Status.PASS , " fiels sorting by text action work Successfully ") ;

		}else {

			logger.log(Status.FAIL , "fiels sorting by text action failed ") ;
		}

		String[] nameFolderArray = {"yosef.txt","file_4.txt"} ;

		DeleteMultipleFolderOrFile(nameFolderArray, false , 0); 

	} 



	// view fiels by size order
	public void viewFielsBySizeOrder() throws InterruptedException, FindFailed 
	{

		uploadFile ("file_4.png" ,"file_4.txt") ;

		uploadFile("file_1.png","yosef.txt") ; 

		WaitForVisibility(allElement1.sortBySize); 

		allElement1.sortBySize.click() ; 

		Thread.sleep(1500) ;  

		if(allElement1.fielsNameList.get(0).getText().equals("yosef.txt") && allElement1.fielsNameList.get(1).getText().equals("file_4.txt")) {

			logger.log(Status.PASS , " fiels sorting by size action work Successfully ") ;
		}
		else {

			logger.log(Status.FAIL , "fiels sorting by size action failed ") ;
		}

		String[] nameFolderArray = {"yosef.txt","file_4.txt"} ;

		DeleteMultipleFolderOrFile(nameFolderArray, false , 0); 		

	}



	// view folders by last change order
	public void viewFoldersByLastChangeOrder(String[] nameFoldersArray) throws InterruptedException 
	{


		for(int i = 0 ; i < nameFoldersArray.length ; i++) {

			CreateFolder(nameFoldersArray[i]) ;   
		}

		String[] checkListOrder  = new String[allElement1.foldersName.size()] ;
		String[] folderListUfterClicking = new String[allElement1.foldersName.size()] ;

		WaitForVisibility(allElement1.sortLastChange); 

		allElement1.sortLastChange.click();
		allElement1.sortLastChange.click();

		Thread.sleep(2000) ; 

		for(int i = 0 ; i < allElement1.foldersName.size() ; i++) {

			folderListUfterClicking[i] = allElement1.foldersName.get(i).getText() ;
		}

		int collect = 0 ;

		for(int i = (nameFoldersArray.length) -1 ; i >= 0 ; i-- ) {

			checkListOrder[collect] = nameFoldersArray[i];

			collect ++ ;

		}

		if(Arrays.equals(checkListOrder, folderListUfterClicking)) { 

			logger.log(Status.PASS , " folder sorting by text action work Successfully ") ;

		}else {

			logger.log(Status.FAIL , "folder sorting by text action failed ") ;
		}

		DeleteMultipleFolderOrFile(nameFoldersArray, false , 0); 	

	}




	// Verify mane page data
	public void verifyManePageData(String[] foldersName) throws InterruptedException, FindFailed 
	{
		String expectedResultLine_1 = "שם: הקבצים שלי" ;
		String expectedResultLine_3 = "מספר קבצים: 2" ;
		String expectedResultLine_4 = "מספר תיקיות: 2" ;


		for(String folder : foldersName) {

			CreateFolder(folder); 
		}

		uploadFile("file_1.png","yosef.txt") ; 

		uploadFile("file_4.png" ,"file_4.txt") ; 

		Thread.sleep(1500) ; 

		allElement1.InfoButton.click();

		WaitForVisibility(allElement1.OkButton);

		if(allElement1.rowsInFileOrFolderInformation.get(0).getText().equals(expectedResultLine_1) 
				&& allElement1.rowsInFileOrFolderInformation.get(2).getText().equals(expectedResultLine_3) 
				&& allElement1.rowsInFileOrFolderInformation.get(3).getText().equals(expectedResultLine_4)) {

			logger.log(Status.PASS , "The validation of the data on the main screen information tab has been successfully completed") ;
		}else {

			logger.log(Status.FAIL , "The validation of the data on the main screen information tab , failed") ; 
		}

		allElement1.OkButton.click();

		DeleteMultipleFolderOrFile(foldersName, false, 0);

	} 



	// verify file information tab
	//		public void verifyFileInformationTab(String fileName) throws InterruptedException { 
	//
	//			sharingFileOrFolderWithReadPermission(fileName, "נייטרו הג'לו", false, 0);
	//
	//			String expectedResultLine_1 = "שם: " + allElements.fielsNameList.get(0).getText() ;
	//			String expectedResultLine_2 = "גודל: 8 B" ;
	//			String expectedResultLine_4 = "שונה לאחרונה: " + allElements.lastFielsModifyList.get(0).getText();
	//			String expectedResultLine_5 = "בעלים: " + allElements.userName.getAttribute("innerText");
	//			
	//			LocateFolder_fileByName(fileName).click();
	//
	//			allElements.InfoButton.click();
	//
	//			WaitForVisibility(allElements.OkButton);
	//			
	//			Thread.sleep(2000) ; 
	//
	//			if(allElements.rowsInFileOrFolderInformation.get(0).getText().equals(expectedResultLine_1)
	//					&& allElements.rowsInFileOrFolderInformation.get(1).getText().equals(expectedResultLine_2)
	//					&& allElements.rowsInFileOrFolderInformation.get(3).getText().equals(expectedResultLine_4)
	//					&& allElements.rowsInFileOrFolderInformation.get(4).getText().equals(expectedResultLine_5)) {
	//
	//				logger.log(Status.PASS , "The validation of the data on the main screen information tab has been successfully completed") ;
	//			}else {
	//
	//				logger.log(Status.FAIL , "The validation of the data on the main screen information tab , failed") ; 
	//			}
	//
	//			try {
	//
	//				if(allElements.shareTage.isDisplayed()) {
	//
	//					logger.log(Status.PASS , "The tag that shows if the file was shared with a user is displayed") ;
	//				}
	//
	//			} catch (Exception e) {
	//				
	//				logger.log(Status.FAIL , "The tag that shows if the file was shared with a user is not displayed") ;
	//			}
	//
	//			allElements.OkButton.click();
	//		}





	// verify folder information tab
	//		public void verifyFolderInformationTab (String folderName) throws InterruptedException  
	//		{
	//			
	//			CreateFolder(folderName); 
	//			
	//			String expectedResultLine_1 = "שם: " + allElements.foldersName.get(0).getText() ;		
	//			String expectedResultLine_3 = "שונה לאחרונה: " + allElements.lastFoldersModifyList.get(0).getText();
	//			String expectedResultLine_4 = "בעלים: " + allElements.userName.getAttribute("innerText");
	//			
	//			sharingFileOrFolderWithReadPermission(folderName , "נייטרו הג'לו", false, 0);
	//			
	//			LocateFolder_fileByName("test1").click(); 
	//			
	//			allElements.InfoButton.click();
	//
	//			WaitForVisibility(allElements.OkButton);
	//			
	//			Thread.sleep(2000) ; 
	//			
	//			if(allElements.rowsInFileOrFolderInformation.get(0).getText().equals(expectedResultLine_1)
	//					&& allElements.rowsInFileOrFolderInformation.get(2).getText().equals(expectedResultLine_3)
	//					&& allElements.rowsInFileOrFolderInformation.get(3).getText().equals(expectedResultLine_4)) {
	//
	//				logger.log(Status.PASS , "The validation of the data on the folder information tab has been successfully completed") ;
	//			}else {
	//
	//				logger.log(Status.FAIL , "The validation of the data on the folder information tab , failed") ; 
	//			}
	//
	//			try {
	//
	//				if(allElements.shareTage.isDisplayed()) {
	//
	//					logger.log(Status.PASS , "The tag that shows if the file was shared with a user is displayed") ;
	//				}
	//
	//			} catch (Exception e) {
	//				
	//				logger.log(Status.FAIL , "The tag that shows if the file was shared with a user is not displayed") ;
	//			}
	//
	//			allElements.OkButton.click();
	//			
	//		}


	// verify folder information tab
	public void verifyMultipleFolderAndFileInformationTab(String[] foldersAndFile) throws InterruptedException  
	{
		String expectedResultLine_1 = allElement1.NumOfItem.size() -1 + " קבצים נבחרו." ;
		String expectedResultLine_2 = "גודל: 214 B" ;

		multipleSelect(foldersAndFile);

		allElement1.InfoButton.click();

		WaitForVisibility(allElement1.OkButton);

		Thread.sleep(2000) ; 

		if(allElement1.rowsInFileOrFolderInformation.get(0).getText().equals(expectedResultLine_1)
				&& allElement1.rowsInFileOrFolderInformation.get(1).getText().equals(expectedResultLine_2)) {

			logger.log(Status.PASS , "The validation of the data on the main screen information tab has been successfully completed") ;
		}else {

			logger.log(Status.FAIL , "The validation of the data on the main screen information tab , failed") ; 
		}

	}


	// clear function 
	public void clear1() 
	{
		action.keyDown(Keys.CONTROL)
		.sendKeys("a")
		.keyUp(Keys.CONTROL)			
		.sendKeys(Keys.BACK_SPACE)
		.build()
		.perform();
	}


	//  Waiting for element on page
	public void WaitForVisibility(WebElement element)  
	{

		wait1.until(ExpectedConditions.visibilityOf(element)) ;

	}



	// function to returns a folder from the folder hierarchy list by name
	public WebElement returnsFolderFromHierarchyList(String nameOfFolder)
	{
		WebElement folderResult = null ;

		for(WebElement folder : allElement1.listOfFolderHierarchy) {

			if(nameOfFolder.equals(folder.getText())) { 

				folderResult = folder ;
				break ;
			}
		}

		return folderResult ; 

	}

	public void rightClick(int index ,String nameOfFolder_File) throws InterruptedException {

		if (nameOfFolder_File != null) {

			action.contextClick(LocateFolder_fileByName(nameOfFolder_File)).perform();
		}
		allElement1.rightClickList.get(index).click();

	}


	// share file or folder with editor permission from user 2
	public void shareFileOrFolder(String file_folderName , String nameOfTheUser) throws InterruptedException 
	{

		LocateFolder_fileByName(file_folderName).click(); 

		WaitForVisibility(allElement1.ShareButton) ; 

		allElement1.ShareButton.click();

		WaitForVisibility(allElement1.searchUserFild) ; 

		allElement1.searchUserFild.click();

		allElement1.searchUserFild.sendKeys(nameOfTheUser) ;

		Thread.sleep(1500) ; 

		action1.sendKeys(Keys.ENTER).build().perform();

		Select select = new Select(allElement1.selectPermissionButton) ;

		select.selectByIndex(1); 

		Thread.sleep(2000) ; 

		WaitForVisibility(allElement1.CreateButtonForShare) ;
			
		allElement1.CreateButtonForShare.click();
	
		Thread.sleep(2000) ;

		driver1.findElement(By.tagName("body")) ;
		
		driver1.navigate().refresh();
		
	} 



}
