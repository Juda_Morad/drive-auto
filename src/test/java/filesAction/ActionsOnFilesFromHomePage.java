package filesAction;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;
import globalElements.AllElements;

public class ActionsOnFilesFromHomePage extends TestBase 
{
	AllElements allElements ;
	GlobalFanction globalFanction ;
	OperationsForTheOtherDriver operationsForTheOtherDriver ;
	
	@BeforeClass
	public void SetUp() throws InterruptedException, IOException {

		allElements = new AllElements();
		globalFanction = new GlobalFanction();
		operationsForTheOtherDriver = new OperationsForTheOtherDriver() ;

	}

	
	
	@Test(priority = 1)
	public void TestUploadFile() throws FindFailed, InterruptedException 
	{
		logger = extent.createTest(" up load file to mane folder") ;
		
		globalFanction.uploadFile("file_1.png","yosef.txt") ; 
		
		globalFanction.uploadFile("file_4.png" ,"file_4.txt") ; 
		
	}
	
	
	@Test(priority = 2)
	public void TestUploadFile_dragAndDrop() throws FindFailed, InterruptedException 
	{
		logger = extent.createTest("up load file to mane folder using drag and drop ") ;
		
		globalFanction.upLoadFile_dragAndDrop("file_2.png","file_3.png","yosef_1.txt");
		
	}
	
	
	
	@Test(priority = 3)
	public void testSharingFileWithReadPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" sharing file from home page with read permission test") ;
		
		globalFanction.sharingFileOrFolderWithReadPermission("yosef.txt","אחמד אדידס" ,"user2","user2pass",false , 0);
	}
	
	 
	
	
	
	@Test(priority = 4)
	public void testSharingFileOrFolderWithEditorPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" sharing file from home page with editor permission test") ;
		
		globalFanction.sharingFileOrFolderWithEditorPermission("yosef.txt","אחמד אדידס" ,"user2","user2pass",false, 0) ;
	}
	
	
	
	@Test(priority = 5)
	public void testRemoveUserPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" renove user permission on file test") ;
		
		globalFanction.removeUserPermission("yosef.txt","אחמד אדידס" ,"user2","user2pass", false, 0) ;
	}
	
	
	
	@Test(priority = 6) 
	public void TestmoveFileToFolder() throws InterruptedException 
	{
	    logger = extent.createTest("move file from home page to folder") ;
	
		globalFanction.MoveFolder_FileToNewLocation("yosef_1.txt","israel", false , 0);
		
	}
	
	
	@Test(priority = 7) 
	public void TestMoveMultipleFilesToFolder() throws InterruptedException 
	{
	    logger = extent.createTest(" test move multiple files from home page to folder ") ;
	    
	    String[] arrayFiels = {"yosef.txt","file_4.txt"};
	    
		globalFanction.MoveMultipleFolder_FileToNewLocation(arrayFiels, "israel", false, 0);  
		
	}
	
	
	
	@Test(priority = 8)
	public void TestrenameFile() throws InterruptedException, FindFailed  
	
	{
		logger = extent.createTest("test rename file from mane folder") ;
		
		globalFanction.uploadFile("file_1.png","yosef.txt") ; 
		
		globalFanction.renameFoldersAndFiles("yosef.txt", "yosef_1.txt", false, 0); 
	}
	
	
	@Test(priority = 9)
	public void TestdownloadFile() throws InterruptedException 
	{
		
		logger = extent.createTest("test download file from mane folder ") ;
		
		globalFanction.downloadFile("yosef_1.txt", false, 0);  
		
	}
	
	
	@Test(priority = 10)
	public void TestdeliteFile() throws FindFailed, InterruptedException 
	{
		logger = extent.createTest(" delite file test from mane folder ") ;
		
		globalFanction.DeleteFolderOrFile("yosef_1.txt", false, 0);  
		
	}


			
	@Test(priority = 11)
	public void  testViewFielsBySizeOrder() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" filtering folder or file by size test") ;
		
		globalFanction.viewFielsBySizeOrder() ; 		
		
	}
	
	
	@Test(priority = 12)
	public void  testVerifyFileinformation() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" verify file data  test") ;
		
		globalFanction.verifyFileInformationTab("yosef.txt" ,"אחמד אדידס" ,"user2","user2pass",false ,0 ) ;
			
	}
	
	
	@Test(priority = 13)
	public void  testVerifyManePageData() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" verify mane page data  test") ;
		
		String[] foldersName = {"test1","test2"} ;
		
		globalFanction.verifyManePageData(foldersName); 
			
	}
	
	
	
	@Test(priority = 14)
	public void  testVerifyMultipleFolderAndFileInformationTab() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest("test verify multiple folder and file information tab") ;
		
		String[] folderAndFileName = {"israel" , "file_4.txt" , "yosef.txt"};
		
		globalFanction.verifyMultipleFolderAndFileInformationTab(folderAndFileName);
			
	}   
	
	
	

	@Test(priority = 15)
	public void testViewFileContents () throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest("test view file contents") ;
		
		globalFanction.viewFileContents("yosef.txt" , false , 0);
		
		globalFanction.DeleteFolderOrFile("file_4.txt", false, 0) ; 
			
	}
	
	
	@Test(priority = 16)
	public void testMoveFileOrFolderToShareWithMeFolder () throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest("test move file to share with me folder") ;
		
		operationsForTheOtherDriver.loginToOtherUser("user2","user2pass");
		
		operationsForTheOtherDriver.CreateFolder("test3");  
		
		operationsForTheOtherDriver.shareFileOrFolder("test3", "נייקי אדידס");
		
		driver1.quit();

		Thread.sleep(1500) ; 
 		
		globalFanction.moveFileOrFolderToShareWithMeFolder("yosef.txt","test3") ;
		
		globalFanction.DeleteFolderOrFile("israel", false , 0) ; 
			
	}
	
	@AfterClass
	public void ufter() 
	{
		extent.flush();
	}
	
	
	
	
}
