package rightClick;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;

public class rightClickTestsFile extends TestBase{

	GlobalFanction globalfunction;

	@BeforeClass
	public void setup() {

		this.globalfunction = new GlobalFanction();
		
	}
	
	@Test(priority = 1)
	public void renameByRightClick() throws InterruptedException, FindFailed {
		
		logger = extent.createTest("rename file right click"); 
		
		globalfunction.uploadFile("file_1.png","yosef.txt") ; 

		globalfunction.renameFoldersAndFiles("yosef.txt", "yosef456",true, 6);
		
	}
	
	@Test(priority = 2)
	public void downloadFileByRightClick() throws InterruptedException, FindFailed {

		logger = extent.createTest("download file right click"); 
		
		globalfunction.downloadFile("yosef456", true, 2);
		
	}
	
	@Test(priority = 3)
	public void shareFileEditorPermission() throws InterruptedException, FindFailed {
		
		logger = extent.createTest(" share file with editor permission right click"); 
		
		globalfunction.sharingFileOrFolderWithEditorPermission("yosef456","אחמד אדידס","user2","user2pass", true, 4);

	}

	@Test(priority = 4)
	public void shareFileReadPermission() throws InterruptedException {

		logger = extent.createTest(" share file with read permission right click"); 
			
		globalfunction.sharingFileOrFolderWithReadPermission("yosef456","אחמד אדידס" ,"user2","user2pass",true , 4);
	}
	
	@Test(priority = 5)
	public void testRemoveUserPermission() throws InterruptedException {
		
		logger = extent.createTest(" remove user permission on file right click"); 
		
		globalfunction.removeUserPermission("yosef456","אחמד אדידס" ,"user2","user2pass", true , 4);
	}
	
	@Test(priority = 6)
	public void moveFileIntoFolder() throws InterruptedException {
		
		logger = extent.createTest("move file into folder right click"); 

		globalfunction.MoveFolder_FileToNewLocation("yosef456", "yaron", true , 5);
		
	}
	
	@Test(priority = 7)
	public void deleteFile() throws InterruptedException, FindFailed {
		
		logger = extent.createTest("delete file right click");
		
		globalfunction.uploadFile("file_1.png","yosef.txt") ; 
		
		globalfunction.DeleteFolderOrFile("yosef.txt", true, 3);

	}
	
	
	@Test(priority = 8)
	public void verifyInformationFiles() throws InterruptedException, FindFailed {
		
		logger = extent.createTest("verify information on files using rigth click");
		
		globalfunction.uploadFile("file_1.png","yosef.txt") ; 
		
		globalfunction.verifyFileInformationTab("yosef.txt","אחמד אדידס","user2","user2pass", true, 0) ; 

	}
	
	
	@Test(priority = 9)
	public void viewFileContent() throws InterruptedException, FindFailed {
		
		logger = extent.createTest(" view file content test , using right click ");
		
		String[] foldersName = {"yosef.txt", "yaron"} ;
		
		globalfunction.viewFileContents("yosef.txt", true, 1) ; 
		
		globalfunction.DeleteMultipleFolderOrFile(foldersName, false, 0);
	}
	
	
	
	@AfterClass
	public void after() 
	{ 
		extent.flush();
	}
	
}
