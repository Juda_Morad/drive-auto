package rightClick;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;

public class rightClickTestsFolders extends TestBase {

	GlobalFanction globalfunction;
	OperationsForTheOtherDriver operationsForTheOtherDriver ;
	
	@BeforeClass
	public void setup() {

		this.globalfunction = new GlobalFanction();
		this.operationsForTheOtherDriver = new OperationsForTheOtherDriver() ;
 
	}

	@Test(priority = 1)
	public void renameByRightClick() throws InterruptedException {

		logger = extent.createTest("rename folder right click"); 

		globalfunction.CreateFolder("juda");

		globalfunction.renameFoldersAndFiles("juda", "juda1", true , 4);
	}

	@Test(priority = 2)
	public void shareFolderEditorPermission() throws InterruptedException {

		logger = extent.createTest(" share folder with editor permission right click"); 

		globalfunction.sharingFileOrFolderWithEditorPermission("juda1","אחמד אדידס","user2","user2pass", true, 2);

	}

	@Test(priority = 3)
	public void shareFolderReadPermission() throws InterruptedException {

		logger = extent.createTest(" share folder with read permission right click"); 
			
		globalfunction.CreateFolder("isr");
		
		globalfunction.sharingFileOrFolderWithReadPermission("isr","אחמד אדידס","user2","user2pass",true , 2);
	}
	
	@Test(priority = 4)
	public void testRemoveUserPermission() throws InterruptedException {

		logger = extent.createTest(" remove user permission right click"); 
		
		globalfunction.removeUserPermission("isr","אחמד אדידס","user2","user2pass", true , 2);
	}
	
	@Test(priority = 5)
	public void moveFolderIntoFolder() throws InterruptedException {
		
		logger = extent.createTest("move folder into folder right click"); 
		
		globalfunction.MoveFolder_FileToNewLocation("juda1", "yaron", true , 3);
		
	}
	
	@Test(priority = 6)
	public void deleteFolder() throws InterruptedException {
		
		logger = extent.createTest("delete folder right click"); 
		
		globalfunction.CreateFolder("ahalan");
		
		globalfunction.DeleteFolderOrFile("ahalan", true, 1);
		
		String[] foldersName = {"isr" ,"yaron"} ;
		
		globalfunction.DeleteMultipleFolderOrFile(foldersName, false, 0);
			
	}
	
	
	@Test(priority = 7)
	public void verifyInformationFolder() throws InterruptedException {
		
		logger = extent.createTest("verify information on folder using right click"); 
		
		globalfunction.verifyFolderInformationTab("yaronShodedZkenot", "אחמד אדידס", "user2","user2pass",false, true, 0);
		
		globalfunction.DeleteFolderOrFile("yaronShodedZkenot", false, 0); 
			
	}
	
	@AfterClass
	public void after() 
	{
		extent.flush();
	}

		
}
