package rightClick;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalElements.AllElements;

public class rightClickFolderFromFolder extends TestBase {

	GlobalFanction globalfunction;
	AllElements allElements ;

	@BeforeClass
	public void setup() {

		this.globalfunction = new GlobalFanction();
		this.allElements = new AllElements() ;
	}
	
	@Test(priority = 1)
	public void TestcreateFolder() throws InterruptedException, FindFailed {
		
		logger = extent.createTest("create Folder for tests folder from folder right click") ;

		globalfunction.CreateFolder("yosefariel47890");

		globalfunction.EnterToFolder("yosefariel47890"); 

		globalfunction.CreateFolder("yosefariel123456789");
	}
	
	@Test(priority = 2)
	public void renameFolders() throws InterruptedException {
	
		logger = extent.createTest("rename Folders from folder rhigh click") ;

		globalfunction.renameFoldersAndFiles("yosefariel123456789", "david",true , 4);
	}	
	
	@Test(priority = 3)
	public void shareFolderEditorPermission() throws InterruptedException {

		logger = extent.createTest(" share folder from folder with editor permission right click"); 

		globalfunction.sharingFileOrFolderWithEditorPermission("david","אחמד אדידס" ,"user2","user2pass", true, 2);
	}
	
	@Test(priority = 4)
	public void shareFolderReadPermission() throws InterruptedException {
		
		logger = extent.createTest(" share folder from folder with read permission right click"); 

		globalfunction.CreateFolder("kigel");
		
		globalfunction.sharingFileOrFolderWithReadPermission("kigel","אחמד אדידס","user2","user2pass",true , 2);

	}

	@Test(priority = 5)
	public void testRemoveUserPermission() throws InterruptedException {
		
		logger = extent.createTest(" remove user permission right click folder from folder");
		
		globalfunction.removeUserPermission("kigel","אחמד אדידס","user2","user2pass" ,true , 2);
	}
	
	@Test(priority = 6)
	public void moveFolderToMainPage() throws InterruptedException {
		
		logger = extent.createTest("move folder to main page right click"); 
		
		globalfunction.MoveFolder_FileToMainPage("yosefariel47890", "kigel", true, 3);

	}
	
	@Test(priority = 7)
	public void deleteFolderFromFolder() throws InterruptedException {
		
		logger = extent.createTest("delete folder from folder right click"); 
		
		globalfunction.DeleteFolderOrFile("david", true, 1);
	
	}
	
	
	@Test(priority = 8)
	public void verifyInformationFolder() throws InterruptedException {
		
		logger = extent.createTest("verify information on folder from folder , using right click"); 
		
		globalfunction.verifyFolderInformationTab("yaronShodedZkenot","אחמד אדידס","user2","user2pass" ,false,true ,0 ) ;
		
		allElements.MyFilesButton.click();  
			
		globalfunction.DeleteFolderOrFile("yosefariel47890", true, 1);
	}
 
	
	@AfterClass
	public void after() 
	{ 
		extent.flush();
	}
	
}
