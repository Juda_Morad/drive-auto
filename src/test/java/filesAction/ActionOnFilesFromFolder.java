package filesAction;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.mongodb.operation.CreateCollectionOperation;

import java.io.IOException;
import java.security.PublicKey;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;
import globalElements.AllElements;

public class ActionOnFilesFromFolder extends TestBase 

{

	AllElements allElements ;
	GlobalFanction globalFanction ;
	OperationsForTheOtherDriver operationsForTheOtherDriver ;


	@BeforeClass
	public void SetUp() throws InterruptedException, IOException {

		allElements = new AllElements();
		globalFanction = new GlobalFanction();
		operationsForTheOtherDriver = new OperationsForTheOtherDriver() ;
	}



	@Test(priority = 1)
	public void TestUploadFileToFolder() throws FindFailed, InterruptedException 
	{
		logger = extent.createTest("Test up load file to folder ") ;
		
		globalFanction.CreateFolder("israel");  
		
		globalFanction.EnterToFolder("israel") ; 
		
		globalFanction.uploadFile("file_1.png","yosef.txt") ; 

		globalFanction.uploadFile("file_4.png" ,"file_4.txt") ; 

	}


	@Test(priority = 2)
	public void TestUploadFile_dragAndDropToFolder() throws FindFailed, InterruptedException 
	{
		logger = extent.createTest(" Test up load file to folder using  drag and drop ") ;

		globalFanction.upLoadFile_dragAndDrop("file_2.png","file_3.png","yosef_1.txt");

	}

	@Test(priority = 3)
	public void TestrenameFile() throws InterruptedException, FindFailed  

	{
		logger = extent.createTest("test rename file from folder ") ; 

		globalFanction.renameFoldersAndFiles("yosef.txt", "yosef_2.txt" , false ,0); 
	}



	@Test(priority = 4)
	public void TestdownloadFile() throws InterruptedException 
	{

		logger = extent.createTest("test download file from folder ") ;

		globalFanction.downloadFile("yosef_1.txt", false , 0);  

	}



	@Test(priority = 5)
	public void testSharingFileOrFolderWithReadPermission() throws InterruptedException 
	{

		logger = extent.createTest(" sharing folder with read permission test") ;

		globalFanction.sharingFileOrFolderWithReadPermission("yosef_1.txt","אחמד אדידס" ,"user2","user2pass",false , 0);
	}



	@Test(priority = 6)
	public void testSharingFileOrFolderWithEditorPermission() throws InterruptedException 
	{

		logger = extent.createTest(" sharing folder with editor permission test") ;

		globalFanction.sharingFileOrFolderWithEditorPermission("yosef_1.txt","אחמד אדידס" ,"user2","user2pass",false, 0) ;
	}



	@Test(priority = 7)
	public void testremoveUserPermission() throws InterruptedException 
	{

		logger = extent.createTest(" sharing folder with editor permission test") ;

		globalFanction.removeUserPermission("yosef_1.txt","אחמד אדידס" ,"user2","user2pass", false, 0) ;
	}




	@Test(priority = 8)  
	public void TestmoveFileToHomePage() throws InterruptedException 
	{
		logger = extent.createTest("move file to mane page ") ;

		globalFanction.MoveFolder_FileToMainPage("israel", "yosef_2.txt" , false , 0);  

	}

	@Test(priority = 9) 
	public void moveMultipleFilesToManePage() throws InterruptedException  
	{
		logger = extent.createTest("move file to mane page") ;

		String[] arrayFiels = {"file_4.txt", "yosef_1.txt"} ;

		globalFanction.MoveMultipleFolder_FileToMainPage("israel" , arrayFiels , false , 0) ;  


	}



	@Test(priority = 10)
	public void  TestMoveMultipleFolder_FileToPreviousFolder() throws FindFailed, InterruptedException 
	{
		logger = extent.createTest("move multiple files to previous folder ") ;

		String[] arrayFiels = {"yosef.txt", "file_4.txt"} ;

		globalFanction.CreateFolder("test_4") ;

		globalFanction.EnterToFolder("test_4"); 

		globalFanction.uploadFile("file_1.png","yosef.txt") ; 

		globalFanction.uploadFile("file_4.png" ,"file_4.txt") ; 

		globalFanction.MoveMultipleFolder_FileToPreviousFolder(arrayFiels ,"test_4", false , 0);

	}


	@Test(priority = 11)
	public void TestdeleteMultipleFile() throws FindFailed, InterruptedException 
	{
		logger = extent.createTest("delete multiple files test fron folder ") ;

		String[] arrayFiels = {"file_4.txt", "yosef.txt"} ;

		globalFanction.DeleteMultipleFolderOrFile(arrayFiels, false , 0) ;  

	}



	@Test(priority = 12)
	public void  testViewFielsBySizeOrder() throws InterruptedException, FindFailed 
	{

		logger = extent.createTest(" filtering folder or file by size test") ;
		
		globalFanction.CreateFolder("test") ; 

		globalFanction.viewFielsBySizeOrder() ; 
	}


	@Test(priority = 13)
	public void  testVerifyFileinformation() throws InterruptedException, FindFailed 
	{

		logger = extent.createTest(" verify file data  test") ;

		globalFanction.verifyFileInformationTab("yosef.txt" ,"אחמד אדידס","user2","user2pass",false ,0 ) ; 

	}


	@Test(priority = 14)
	public void  testVerifyMultipleFolderAndFileInformationTab() throws InterruptedException, FindFailed 
	{

		logger = extent.createTest(" verify multiple fiels data test") ;

		String[] folderAndFileName = {"test" , "file_4.txt" , "yosef.txt" }; 
		
		globalFanction.verifyMultipleFolderAndFileInformationTab(folderAndFileName);

	}


	@Test(priority = 15)
	public void testViewFileContents () throws InterruptedException, FindFailed 
	{

		logger = extent.createTest("test view file contents") ;

		globalFanction.viewFileContents("yosef.txt" , false , 0);

	}


	@Test(priority = 16)
	public void testMoveFileOrFolderToShareWithMeFolder () throws InterruptedException, FindFailed 
	{

		logger = extent.createTest("test move file or folder to share with me folder") ;

		operationsForTheOtherDriver.loginToOtherUser("user2","user2pass");

		operationsForTheOtherDriver.CreateFolder("test7");  

		operationsForTheOtherDriver.shareFileOrFolder("test7", "נייקי אדידס");

		driver1.quit();

		Thread.sleep(1500) ; 

		globalFanction.moveFileOrFolderToShareWithMeFolder("yosef.txt","test7") ;
		
		globalFanction.DeleteFolderOrFile("israel", false , 0) ; 

	}




	@AfterClass
	public void after() 
	{
		extent.flush();
	}






}
