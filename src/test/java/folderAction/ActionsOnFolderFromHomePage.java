package folderAction;

import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;
import globalElements.AllElements;

public class ActionsOnFolderFromHomePage extends TestBase

{

	AllElements allElements;
	GlobalFanction globalFanction;
	OperationsForTheOtherDriver operationsForTheOtherDriver ;

	

	@BeforeClass
	public void SetUp() throws InterruptedException, IOException {

		this.allElements = new AllElements();
		this.globalFanction = new GlobalFanction();
		this.operationsForTheOtherDriver = new OperationsForTheOtherDriver() ;

	}

	@Test(priority = 1)
	public void TestCreateFolder() throws InterruptedException {
		
		logger = extent.createTest("Test create Folder from home page");

		globalFanction.CreateFolder("yosefFolder");

	}
	
	@Test(priority = 2)
	public void testSharingFolderWithReadPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" sharing folder from home page with read permission test") ;
		
		globalFanction.sharingFileOrFolderWithReadPermission("yosefFolder","אחמד אדידס" ,"user2","user2pass",false , 0);
	}
	
	@Test(priority = 3)
	public void testSharingFolderWithEditorPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" sharing folder from home page with editor permission test") ;
		
		globalFanction.sharingFileOrFolderWithEditorPermission("yosefFolder","אחמד אדידס" ,"user2","user2pass",false, 0) ;
	}
	
	
	@Test(priority = 4)
	public void testremoveUserPermission() throws InterruptedException 
	{
		
		logger = extent.createTest(" removing user sharing folder from home page test") ;
		
		globalFanction.removeUserPermission("yosefFolder","אחמד אדידס" ,"user2","user2pass", false, 0) ;
	}

	
	@Test(priority = 5 , dependsOnMethods = {"TestCreateFolder"})
	public void renameFolders() throws InterruptedException

	{
		logger = extent.createTest("rename Folder from home page");
				
		globalFanction.renameFoldersAndFiles("yosefFolder", "yosefFolder1", false, 0);
		
		globalFanction.DeleteFolderOrFile("yosefFolder1", false, 0);
	}

	@Test(priority = 6)
	public void moveFolderToFolder() throws InterruptedException {
		logger = extent.createTest("move folder from home page to folder ");

		globalFanction.CreateFolder("yosefFolder1");

		globalFanction.MoveFolder_FileToNewLocation("yosefFolder1", "ariel", false, 0);

	}

	@Test(priority = 7)
	public void moveMultipleFoldersToFolder() throws InterruptedException {
		logger = extent.createTest("move multiple folders from home page to folder");
		
		String[] arrayFiels = {"yosefFolder2", "yosefFolder3"}; 

		globalFanction.CreateFolder("yosefFolder2");

		globalFanction.CreateFolder("yosefFolder3");

		globalFanction.MoveMultipleFolder_FileToNewLocation(arrayFiels, "ariel", false, 0);

	}
															 																																
	@Test(priority = 8)
	public void switchViewTest() throws InterruptedException {

		logger = extent.createTest("switch view Test");

		globalFanction.SwitchView(); 
		
		globalFanction.DeleteFolderOrFile("ariel", false, 0) ; 

	}

	@Test(priority = 9)
	public void deleteMultipleFolders() throws InterruptedException {
		
		logger = extent.createTest("delete multiple Folder from home page");
		
		String[] arrayFiels = {"yosefTest5", "yosefTest6", "yosefTest7"};  
		
		globalFanction.CreateFolder("yosefTest5");
		globalFanction.CreateFolder("yosefTest6");
		globalFanction.CreateFolder("yosefTest7");

		globalFanction.DeleteMultipleFolderOrFile(arrayFiels, false, 0);

	}
	
	
	@Test(priority = 10)
	public void  testViewFoldersInAlphaBetaOrder() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" filtering folder from home page by name test") ;
		
		String[] foldersName = {"testb" ,"testa" ,"testz"} ;
		
		globalFanction.viewFoldersInAlphaBetaOrder(foldersName);
		
		
	}
	
	@Test(priority = 11)
	public void  testViewFoldersByLastChange() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" test filtering folder from home page by last change ") ;
		
		String[] foldersName = {"test1","test2","test3"} ;
		
		globalFanction.viewFoldersByLastChangeOrder(foldersName) ; 
		
		
	}


	
	@Test(priority = 12)
	public void  testVerifyFolderinformation() throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest(" test verify folder data from home page ") ;
		
		globalFanction.verifyFolderInformationTab("test10","אחמד אדידס" ,"user2","user2pass",false,false ,0 ) ;
			
	}
	
	
	
	
	@Test(priority = 13)
	public void testMoveFileOrFolderToShareWithMeFolder () throws InterruptedException, FindFailed 
	{
		 
		logger = extent.createTest("test move folder from home page to share with me folder") ;
		
		operationsForTheOtherDriver.loginToOtherUser("user2","user2pass");
		
		operationsForTheOtherDriver.CreateFolder("test4");  
		
		operationsForTheOtherDriver.shareFileOrFolder("test4", "נייקי אדידס");
		
		driver1.quit();
		
		Thread.sleep(1500) ; 
		
		globalFanction.moveFileOrFolderToShareWithMeFolder("test10","test4") ;
			
	}
	
	
	@AfterClass
	public void after() 
	{
		extent.flush();
	}

}
