package uploadAllFileTypes;

import java.io.IOException;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import base.TestBase;
import globalAction.GlobalFanction;
import globalAction.OperationsForTheOtherDriver;
import globalElements.AllElements;

public class UploadAllFileTypesTest extends TestBase {

	AllElements allElements;
	GlobalFanction globalFanction;
	OperationsForTheOtherDriver operationsForTheOtherDriver;


	@BeforeClass
	public void SetUp() throws InterruptedException, IOException {

		this.allElements = new AllElements();
		this.globalFanction = new GlobalFanction();
		operationsForTheOtherDriver = new OperationsForTheOtherDriver();
		
		Thread.sleep(2000) ; 
		
		allElements.MyFilesButton.click();
		Thread.sleep(2000) ;
	}	

	@Test(priority = 1)
	public void TestUploadFile1() throws FindFailed, InterruptedException {
		logger = extent.createTest(" up load xlsx file");

		globalFanction.uploadFile("xlsx.png", "xlsx.xlsx");
		
		globalFanction.viewFileContents("xlsx.xlsx", false, 0);  
		
		globalFanction.DeleteFolderOrFile("xlsx.xlsx", false, 0);  

	}


	@Test(priority = 2) public void TestUploadFile2() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load XLSX file") ;

	globalFanction.uploadFile("XLSX.png" ,"XLSX.XLSX") ;
	
	globalFanction.viewFileContents("XLSX.XLSX", false, 0);
	
	globalFanction.DeleteFolderOrFile("XLSX.XLSX", false, 0);  

	}


	@Test(priority = 3) public void TestUploadFile3() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load PPT.PPT file") ;

	globalFanction.uploadFile("PPT.png" ,"PPT.PPT") ;
	
	globalFanction.viewFileContents("PPT.PPT", false, 0);
	
	globalFanction.DeleteFolderOrFile("PPT.PPT", false, 0);  

	}

	@Test(priority = 4) public void TestUploadFile4() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load ppt.ppt file") ;

	globalFanction.uploadFile("ppt.png" ,"ppt.ppt") ;
	
	globalFanction.viewFileContents("ppt.ppt", false, 0);
	
	globalFanction.DeleteFolderOrFile("ppt.ppt", false, 0);  

	}


	@Test(priority = 5) public void TestUploadFile5() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load pptx.pptx file") ;

	globalFanction.uploadFile("pptx.png" ,"pptx.pptx") ;
	
	globalFanction.viewFileContents("pptx.pptx", false, 0);
	
	globalFanction.DeleteFolderOrFile("pptx.pptx", false, 0);  

	}


	@Test(priority = 6) public void TestUploadFile6() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load PPTX.PPTX file") ;

	globalFanction.uploadFile("PPTX.png" ,"PPTX.PPTX") ;
	
	globalFanction.viewFileContents("PPTX.PPTX", false, 0);
	
	globalFanction.DeleteFolderOrFile("PPTX.PPTX", false, 0);  

	}


	@Test(priority = 7) public void TestUploadFile7() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load DOC.DOC file") ;

	globalFanction.uploadFile("DOC.png" ,"DOC.DOC") ;
	
	globalFanction.viewFileContents("DOC.DOC", false, 0);
	
	globalFanction.DeleteFolderOrFile("DOC.DOC", false, 0);  

	}


	@Test(priority = 8) public void TestUploadFile8() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load doc.doc file") ;

	globalFanction.uploadFile("doc.png" ,"doc.doc") ;
	
	globalFanction.viewFileContents("doc.doc", false, 0);
	
	globalFanction.DeleteFolderOrFile("doc.doc", false, 0);  

	}


	@Test(priority = 9) public void TestUploadFile9() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load docx.docx file") ;

	globalFanction.uploadFile("docx.png" ,"docx.docx") ;
	
	globalFanction.viewFileContents("docx.docx", false, 0);
	
	globalFanction.DeleteFolderOrFile("docx.docx", false, 0);  

	}


	@Test(priority = 10) public void TestUploadFile10() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load DOCX.DOCX file") ;

	globalFanction.uploadFile("DOCX.png" ,"DOCX.DOCX") ;
	
	globalFanction.viewFileContents("DOCX.DOCX", false, 0);
	
	globalFanction.DeleteFolderOrFile("DOCX.DOCX", false, 0);  

	}


	@Test(priority = 11) public void TestUploadFile11() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load pdf.pdf file") ;

	globalFanction.uploadFile("pdf.png" ,"pdf.pdf") ;
	
	//globalFanction.viewFileContents("pdf.pdf", false, 0);
	
	globalFanction.DeleteFolderOrFile("pdf.pdf", false, 0);  

	}


	@Test(priority = 12) public void TestUploadFile12() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load PDF.PDF file") ;

	globalFanction.uploadFile("PDF.png" ,"PDF.PDF") ;

	//globalFanction.viewFileContents("PDF.PDF", false, 0);
	
	globalFanction.DeleteFolderOrFile("PDF.PDF", false, 0);  

	}


	@Test(priority = 13) public void TestUploadFile13() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load wave.wave file") ;

	globalFanction.uploadFile("wave.png" ,"wave.wave") ;
	
	globalFanction.viewFileContents("wave.wave", false, 0);
	
	globalFanction.DeleteFolderOrFile("wave.wave", false, 0);  

	}


	@Test(priority = 14) public void TestUploadFile14() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load WAVE.WAVE file") ;

	globalFanction.uploadFile("WAVE.png" ,"WAVE.WAVE") ;
	
	globalFanction.viewFileContents("WAVE.WAVE", false, 0);
	
	globalFanction.DeleteFolderOrFile("WAVE.WAVE", false, 0);  

	}


	@Test(priority = 15) public void TestUploadFile15() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load wav.wav file") ;

	globalFanction.uploadFile("wav.png" ,"wav.wav") ;
	
	globalFanction.viewFileContents("wav.wav", false, 0);

	globalFanction.DeleteFolderOrFile("wav.wav", false, 0);  
	
	}


	@Test(priority = 16) public void TestUploadFile16() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load WAV.WAV file") ;

	globalFanction.uploadFile("WAV.png" ,"WAV.WAV") ;
	
	globalFanction.viewFileContents("WAV.WAV", false, 0);
	
	globalFanction.DeleteFolderOrFile("WAV.WAV", false, 0);  

	}


	@Test(priority = 17) public void TestUploadFile17() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load MP3.MP3 file") ;

	globalFanction.uploadFile("MP3.png" ,"MP3.MP3") ;
	
	globalFanction.viewFileContents("MP3.MP3", false, 0);
	
	globalFanction.DeleteFolderOrFile("MP3.MP3", false, 0);  

	}


	@Test(priority = 18) public void TestUploadFile18() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load mp3.mp3 file") ;

	globalFanction.uploadFile("mp3.png" ,"mp3.mp3") ;
	
	globalFanction.viewFileContents("mp3.mp3", false, 0);
	
	globalFanction.DeleteFolderOrFile("mp3.mp3", false, 0);  

	}


	@Test(priority = 19) public void TestUploadFile19() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load mp4.mp4 file") ;

	globalFanction.uploadFile("mp4.png" ,"mp4.mp4") ;
	
	globalFanction.viewFileContents("mp4.mp4", false, 0);
	
	globalFanction.DeleteFolderOrFile("mp4.mp4", false, 0);  

	}


	@Test(priority = 20) public void TestUploadFile20() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load MP4.MP4 file") ;

	globalFanction.uploadFile("MP4.png" ,"MP4.MP4") ;
	
	globalFanction.viewFileContents("MP4.MP4", false, 0);
	
	globalFanction.DeleteFolderOrFile("MP4.MP4", false, 0);  

	}


	@Test(priority = 21) public void TestUploadFile21() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load gif.gif file") ;

	globalFanction.uploadFile("gif.png" ,"gif.gif") ;
	
	globalFanction.viewFileContents("gif.gif", false, 0);
	
	globalFanction.DeleteFolderOrFile("gif.gif", false, 0);  

	}


	@Test(priority = 22) public void TestUploadFile22() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load GIF.GIF file") ;

	globalFanction.uploadFile("GIF.png" ,"GIF.GIF") ;
	
	globalFanction.viewFileContents("GIF.GIF", false, 0);
	
	globalFanction.DeleteFolderOrFile("GIF.GIF", false, 0);  

	}


	@Test(priority = 23) public void TestUploadFile23() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load bmp.bmp file") ;

	globalFanction.uploadFile("bmp.png" ,"bmp.bmp") ;
	
	globalFanction.viewFileContents("bmp.bmp", false, 0);
	
	globalFanction.DeleteFolderOrFile("bmp.bmp", false, 0);  

	}



	@Test(priority = 24) public void TestUploadFile24() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load BMP.BMP file") ;

	globalFanction.uploadFile("BMP.png" ,"BMP.BMP") ;
	
	globalFanction.viewFileContents("BMP.BMP", false, 0);
	
	globalFanction.DeleteFolderOrFile("BMP.BMP", false, 0);  

	}


	@Test(priority = 25) public void TestUploadFile25() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load mpeg.mpeg file") ;

	globalFanction.uploadFile("mpeg.png" ,"mpeg.mpeg") ;
	
	globalFanction.viewFileContents("mpeg.mpeg", false, 0);
	
	globalFanction.DeleteFolderOrFile("mpeg.mpeg", false, 0);  

	}


	@Test(priority = 26) public void TestUploadFile26() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load MPEG.MPEG file") ;

	globalFanction.uploadFile("MPEG.png" ,"MPEG.MPEG") ;
	
	globalFanction.viewFileContents("MPEG.MPEG", false, 0);
	
	globalFanction.DeleteFolderOrFile("MPEG.MPEG", false, 0);  

	}


	@Test(priority = 27) public void TestUploadFile27() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load mpg.mpg file") ;

	globalFanction.uploadFile("mpg.png" ,"mpg.mpg") ;
	
	globalFanction.viewFileContents("mpg.mpg", false, 0);
	
	globalFanction.DeleteFolderOrFile("mpg.mpg", false, 0);  

	}


	@Test(priority = 28) public void TestUploadFile28() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load MPG.MPG file") ;

	globalFanction.uploadFile("MPG.png" ,"MPG.MPG") ;
	
	globalFanction.viewFileContents("MPG.MPG", false, 0);
	
	globalFanction.DeleteFolderOrFile("MPG.MPG", false, 0);  

	}


	@Test(priority = 29) public void TestUploadFile29() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load png.png file") ;

	globalFanction.uploadFile("png2.png" ,"png.png") ;
	
	globalFanction.viewFileContents("png.png", false, 0);
	
	globalFanction.DeleteFolderOrFile("png.png", false, 0);  
	
	}


	@Test(priority = 30) public void TestUploadFile30() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load PNG.PNG file") ;

	globalFanction.uploadFile("PNG.png" ,"PNG.PNG") ;
	
	globalFanction.viewFileContents("PNG.PNG", false, 0);
	
	globalFanction.DeleteFolderOrFile("PNG.PNG", false, 0);  

	}


	@Test(priority = 31) public void TestUploadFile31() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load TIF.TIF file") ;

	globalFanction.uploadFile("TIF.png" ,"TIF.TIF") ;
	
	globalFanction.viewFileContents("TIF.TIF", false, 0);
	
	globalFanction.DeleteFolderOrFile("TIF.TIF", false, 0);  

	}


	@Test(priority = 32) public void TestUploadFile32() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load tif.tif file") ;

	globalFanction.uploadFile("tif.png" ,"tif.tif") ;
	
	globalFanction.viewFileContents("tif.tif", false, 0);
	
	globalFanction.DeleteFolderOrFile("tif.tif", false, 0);  

	}


	@Test(priority = 33) public void TestUploadFile33() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load jpe.jpe file") ;

	globalFanction.uploadFile("jpe.png" ,"jpe.jpe") ;
	
	globalFanction.viewFileContents("jpe.jpe", false, 0);
	
	globalFanction.DeleteFolderOrFile("jpe.jpe", false, 0);  

	}


	@Test(priority = 34) public void TestUploadFile34() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load JPE.JPE file") ;

	globalFanction.uploadFile("JPE.png" ,"JPE.JPE") ;
	
	globalFanction.viewFileContents("JPE.JPE", false, 0);
	
	globalFanction.DeleteFolderOrFile("JPE.JPE", false, 0);  

	}


	@Test(priority = 35) public void TestUploadFile35() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load jpeg.jpeg file") ;

	globalFanction.uploadFile("jpeg.png" ,"jpeg.jpeg") ;
	
	globalFanction.viewFileContents("jpeg.jpeg", false, 0);
	
	globalFanction.DeleteFolderOrFile("jpeg.jpeg", false, 0);  

	}


	@Test(priority = 36) public void TestUploadFile36() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load JPEG.JPEG file") ;

	globalFanction.uploadFile("JPEG.png" ,"JPEG.JPEG") ;
	
	globalFanction.viewFileContents("JPEG.JPEG", false, 0);
	
	globalFanction.DeleteFolderOrFile("JPEG.JPEG", false, 0);  

	}


	@Test(priority = 37) public void TestUploadFile37() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load JPG.JPG file") ;

	globalFanction.uploadFile("JPG.png" ,"JPG.JPG") ;
	
	globalFanction.viewFileContents("JPG.JPG", false, 0);
	
	globalFanction.DeleteFolderOrFile("JPG.JPG", false, 0);  

	}


	@Test(priority = 38) public void TestUploadFile38() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load jpg.jpg file") ;

	globalFanction.uploadFile("jpg.png" ,"jpg.jpg") ;
	
	globalFanction.viewFileContents("jpg.jpg", false, 0);
	
	globalFanction.DeleteFolderOrFile("jpg.jpg", false, 0);  

	}


	@Test(priority = 39) public void TestUploadFile39() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load csv.csv file") ;

	globalFanction.uploadFile("csv.png" ,"csv.csv") ;
	
	globalFanction.viewFileContents("csv.csv", false, 0);
	
	globalFanction.DeleteFolderOrFile("csv.csv", false, 0);  

	}


	@Test(priority = 40) public void TestUploadFile40() throws FindFailed,
	InterruptedException { logger = extent.createTest(" up load CSV.CSV file") ;

	globalFanction.uploadFile("CSV.png" ,"CSV.CSV") ;
	
	globalFanction.viewFileContents("CSV.CSV", false, 0);
	
	globalFanction.DeleteFolderOrFile("CSV.CSV", false, 0);  

	}


	@AfterClass
	public void after() throws InterruptedException {
		Thread.sleep(3000);

		extent.flush();
	}

}
